﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Models
{

    public class DummySearch
    {
        [StringLength(40), Display(Name = "Customer Name")]
        public string customerName { get; set; }

        [StringLength(40), Display(Name = "Payer Email")]
        public string payerEmail { get; set; }

        [StringLength(40), Display(Name = "Policy Owner Email")]
        public string poEmail { get; set; }
        
        [StringLength(40), Display(Name = "Transaction ID")]
        public string TransactionID { get; set; }

        [StringLength(20), Display(Name = "Payo ID")]
        public string payoid { get; set; }

        [StringLength(20), Display(Name = "Policy Number")]
        public string policyNumber { get; set; }

        [StringLength(20), Display(Name = "Type")]
        public string type { get; set; }

        [StringLength(20), Display(Name = "Gateway")]
        public string gateways { get; set; }

        [Display(Name = "Download Status")]
        public int downloadStatus { get; set; }

        [Display(Name = "Status")]
        public string status { get; set; }

        [Display(Name = "Payment From Date")]
        public string startDate { get; set; }

        [Display(Name = "Payment To Date")]
        public string endDate { get; set; }

        public IEnumerable<SelectListItem> DropDownListForPaymentType { get; set; }
        public IEnumerable<SelectListItem> DropDownListForGateways { get; set; }
    }
}