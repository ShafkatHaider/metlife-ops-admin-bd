﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Models
{
    public class DummyUserRole
    {

        [Display(Name = "User")]
        public string userName { get; set; }
        public Nullable<int> ROLE_ID { get; set; }

        public IEnumerable<SelectListItem> DropDownListForRole { get; set; }
    }

}