﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MetlifeAdminPanel.Models
{
    public class Login
    {
        [Display(Name = "Username")]
        public string userName { get; set; }


        [Display(Name = "Password")]
        public string password { get; set; }

    }
}