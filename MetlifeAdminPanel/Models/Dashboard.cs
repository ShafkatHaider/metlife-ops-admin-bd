﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetlifeAdminPanel.Models
{
    public class Dashboard
    {
        public int initCount;
        public int successCount;
        public int failedCount;
        public int canceledCount;
        public int totaluploaded;
        public decimal  totalamountuploaded;
        public string uploadeddatetime;
        public string uploadedby;
    }
}