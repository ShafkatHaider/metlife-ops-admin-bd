﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Models
{

    public class DummySubmissionSearch
    {
        [StringLength(40), Display(Name = "Policy Number")]
        public string PolicyNumber { get; set; }

        [StringLength(40), Display(Name = "YearOfBirth")]
        public string YearOfBirth { get; set; }

        [StringLength(40), Display(Name = "Policy Owner Name")]
        public string POName { get; set; }
        
        [StringLength(40), Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(20), Display(Name = "Phone")]
        public string Phone { get; set; }

        [StringLength(20), Display(Name = "Benefit Type")]
        public string BenefitType { get; set; }

        [Display(Name = "From Date")]
        public string startDate { get; set; }

        [Display(Name = "To Date")]
        public string endDate { get; set; }


        [Display(Name = "Download Status")]
        public int downloadStatus { get; set; }

     }
}