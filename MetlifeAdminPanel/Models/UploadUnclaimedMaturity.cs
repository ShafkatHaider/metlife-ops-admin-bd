﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Models
{

    public class UploadUnclaimedMaturity
    {
        /*
        [StringLength(40), Display(Name = "File")]
        public string uploadFile { get; set; }
        */
        [Required, FileExtensions(ErrorMessage = "excel file required")]
        public HttpPostedFileBase file { get; set; }

    }
}