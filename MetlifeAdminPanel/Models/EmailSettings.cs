﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MetlifeAdminPanel.Models
{
    public  class EmailSettings
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string MailServer { get; set; }
        public string Port { get; set; }
        public string FromEmail { get; set; }
        public string ToEmail { get; set; }
        public string CC { get; set; }
    }
}