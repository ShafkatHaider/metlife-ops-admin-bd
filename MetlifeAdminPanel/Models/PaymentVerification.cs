﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MetlifeAdminPanel.Models
{
    public class PaymentVerification
    {
        [StringLength(40), Display(Name = "Policy Number")]
        public string policyNumber { get; set; }

        [StringLength(40), Display(Name = "Year of Birth")]
        public string yearOfBirth { get; set; }
    }
}