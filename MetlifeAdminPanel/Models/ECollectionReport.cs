﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Models
{
    public class ECollectionReport
    {
        [Display(Name = "Order ID")]
        public string orderId { get; set; }

        [Display(Name = "Beneficiary Name")]
        public string beneficiaryName { get; set; }

        [Display(Name = "Bank A/C Number")]
        public string bankAccountNumber { get; set; }

        [Display(Name = "From Amount")]
        public string fromAmount  { get; set; }

        [Display(Name = "To Amount")]
        public string toAmount { get; set; }

        [StringLength(20), Display(Name = "Paying Bank")]
        public string payingBank { get; set; }

        public IEnumerable<SelectListItem> DropDownListForECollectionBank { get; set; }
    }
}