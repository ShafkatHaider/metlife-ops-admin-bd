//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MetlifeAdminPanel
{
    using System;
    using System.Collections.Generic;
    
    public partial class LLBOLPAY
    {
        public int ID { get; set; }
        public string OPCO { get; set; }
        public byte[] OPPOLNUM { get; set; }
        public Nullable<decimal> OPMODE { get; set; }
        public byte[] OPPAYOR { get; set; }
        public string OPSTATUS { get; set; }
        public Nullable<decimal> OPPREMIUM { get; set; }
        public Nullable<decimal> OPPREMDBO { get; set; }
        public string OPLOANDT { get; set; }
        public Nullable<decimal> OPLOANBL { get; set; }
        public Nullable<decimal> OPAPLBL { get; set; }
        public Nullable<decimal> OPRIREQ { get; set; }
        public string OPPAIDDT { get; set; }
        public string OPPAIDDT1 { get; set; }
        public string OPPAIDDT2 { get; set; }
        public string OPPAIDDT3 { get; set; }
        public string OPMATDT { get; set; }
        public byte[] OPAGENT { get; set; }
        public string OPPRODUCT { get; set; }
        public string OPTDRST { get; set; }
        public string OPDT { get; set; }
        public byte[] OPBTHDT { get; set; }
        public byte[] OPEMAIL { get; set; }
        public byte[] OPMOBNO { get; set; }
    }
}
