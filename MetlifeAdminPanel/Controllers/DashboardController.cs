﻿using MetlifeAdminPanel.Models;
using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class DashboardController : Controller
    {

        public ActionResult show()
        {
            List<Dashboard> dashboardCountsForToday = DBContext.DashboardCountForToday();
            List<Dashboard> dashboardCountsTotal = DBContext.DashboardCountTotal();
            List<Dashboard> dashboardunclaimedmaturity = DBContext.DashboardCountForUnclaimed();


            DashboardManager dbManager = new DashboardManager();
            dbManager.countForToday = dashboardCountsForToday;
            dbManager.countTotal = dashboardCountsTotal;
            dbManager.unclaimedmaturityupload = dashboardunclaimedmaturity;
            return View(dbManager);
        }

    }
}
