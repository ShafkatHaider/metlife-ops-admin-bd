﻿using MetlifeAdminPanel.Services;
using RKLib.ExportData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class PaymentVerificationController : Controller
    {
        List<DisplayTable> listPaymentType = DBContext.GetPaymentTypeList();
        public static string downloadedFileName = "";
        string path = "Downloaded/";
        // GET: /Report/Search
        public ActionResult Search()
        {
            if (!DBContext.hasPermission("Metlife Payment Verification Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                PaymentVerificationManager reportManager = new PaymentVerificationManager();
                return View(reportManager);
            }
        }

        // POST: /Report/Search
        [HttpPost]
        public ActionResult Search(PaymentVerificationManager reportManager)
        {
            if (!DBContext.hasPermission("Metlife Payment Verification Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                List<Transaction> list = DBContext.GetTransactionList(reportManager);
                reportManager.tranList = list;
                return View(reportManager);
            }
        }
   }
}
