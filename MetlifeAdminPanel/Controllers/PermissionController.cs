﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class PermissionController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /Permission/

        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                var permissions = db.Permissions.Include(p => p.Menu);
                return View(permissions.ToList());
            }
        }

        //
        // GET: /Permission/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Permission permission = db.Permissions.Find(id);
                if (permission == null)
                {
                    return HttpNotFound();
                }
                return View(permission);
            }
        }

        //
        // GET: /Permission/Create

        List<DisplayTable> list = DBContext.GetMenuList();
        public ActionResult Create()
        {
            if (!DBContext.hasPermission("Metlife Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                var permission = new Permission();
                permission.DropDownList = new SelectList(list, "Key", "Display");

                // ViewBag.MENU_ID = new SelectList(db.Menus, "ID", "URL");
                return View(permission);
            }
        }

        //
        // POST: /Permission/Create

        [HttpPost]
        public ActionResult Create(Permission permission)
        {
            if (!DBContext.hasPermission("Metlife Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    permission.IS_ACTIVE = 1;
                    db.Permissions.Add(permission);
                    db.SaveChanges();
                    TempData["success"] = "Permission created successfully";
                    return RedirectToAction("Index");
                }
                else {
                    TempData["failed"] = "Permission registration failed";
                }

                ViewBag.MENU_ID = new SelectList(db.Menus, "ID", "URL", permission.MENU_ID);
                return View(permission);
            }
        }

        //
        // GET: /Permission/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Permission permission = db.Permissions.Find(id);
                if (permission == null)
                {
                    return HttpNotFound();
                }
                ViewBag.MENU_ID = new SelectList(db.Menus, "ID", "URL", permission.MENU_ID);
                return View(permission);
            }
        }

        //
        // POST: /Permission/Edit/5

        [HttpPost]
        public ActionResult Edit(Permission permission)
        {
            if (!DBContext.hasPermission("Metlife Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    db.Entry(permission).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["success"] = "Permission updated successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["failed"] = "Permission update failed";
                }
                ViewBag.MENU_ID = new SelectList(db.Menus, "ID", "URL", permission.MENU_ID);
                return View(permission);
            }
        }

        //
        // GET: /Permission/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Permission permission = db.Permissions.Find(id);
                if (permission == null)
                {
                    return HttpNotFound();
                }
                return View(permission);
            }
        }

        //
        // POST: /Permission/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!DBContext.hasPermission("Metlife Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Permission permission = db.Permissions.Find(id);
                db.Permissions.Remove(permission);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}