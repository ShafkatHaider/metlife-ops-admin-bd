﻿using MetlifeAdminPanel.Models;
using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class UnclaimedMaturityController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
        // GET: /Upload
        public ActionResult Upload()
        {
            if (!DBContext.hasPermission("Metlife Upload Unclaimed Maturity File"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                UploadUnclaimedMaturity reportManager = new UploadUnclaimedMaturity();
                return View(reportManager);
            }
        }

        // GET: /UploadScheduler
        public ActionResult UploadScheduler()
        {
            try
            {
                double elapsed_time = 0;
                List<UnclaimedMaturityLog> listPendingUnclaimedMaturityLog = DBContext.GetPendingMaturityFileList();

                if (listPendingUnclaimedMaturityLog != null)
                {
                    foreach (UnclaimedMaturityLog unclaimedMaturityLog in listPendingUnclaimedMaturityLog)
                    {
                        string path = Server.MapPath("~/UploadedFiles/") + unclaimedMaturityLog.FileName;
                        ViewBag.Message = path;

                        OleDbConnection con_excel = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 8.0");
                        if (con_excel.State == ConnectionState.Closed)
                            con_excel.Open();

                        OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", con_excel);
                        OleDbDataReader reader = cmd.ExecuteReader();
                        Enc enc = new Enc();
                        if (reader.HasRows)
                        {
                            OLPABDDATAEntities_new context = new OLPABDDATAEntities_new();
                            List<UnclaimedMaturityData> dataList = new List<UnclaimedMaturityData>();
                            
                            context.Configuration.AutoDetectChangesEnabled = false; //new
                            var stopwatch = new Stopwatch();
                            stopwatch.Start();


                            


                            int i = 0;
                            while (reader.Read())
                            {

                                string POLICY_NO = reader.GetValue(0).ToString();
                                string POLICY_OWNER_NAME = reader.GetValue(1).ToString();
                                string YEAR_OF_BIRTH = reader.GetValue(2).ToString();
                                string AMOUNT = reader.GetValue(3).ToString();
                                string BENEFIT_TYPE = reader.GetValue(4).ToString();
                                UnclaimedMaturityData unclaimedMaturityData = new UnclaimedMaturityData();
                                unclaimedMaturityData.BatchID = unclaimedMaturityLog.BachId.ToString();
                                unclaimedMaturityData.PolicyNo = enc.Encrypt(POLICY_NO);
                                unclaimedMaturityData.POName = enc.Encrypt(POLICY_OWNER_NAME);
                                unclaimedMaturityData.YearOfBirth = enc.Encrypt(YEAR_OF_BIRTH);
                                unclaimedMaturityData.Amount = Convert.ToDecimal(AMOUNT);
                                unclaimedMaturityData.BenefitType = BENEFIT_TYPE;
                                unclaimedMaturityData.CreationTime = DateTime.Now;
                                dataList.Add(unclaimedMaturityData);
                                //db.UnclaimedMaturityData.Add(unclaimedMaturityData);
                                //db.SaveChanges();
                                i++;
                            }
                            //test comment

                            //db.UnclaimedMaturityData.AddRange(dataList);
                            //db.SaveChanges();
                            //unclaimedMaturityLog.Status = "SUCCESS";
                            //db.Entry(unclaimedMaturityLog).State = EntityState.Modified;
                            //db.SaveChanges();

                            //test comment
                            




                            //new
                            List<UnclaimedMaturityData> accounts = new List<UnclaimedMaturityData>();
                            foreach (var account in dataList)
                            {
                                accounts.Add(account);
                                if (accounts.Count % 10000 == 0)
                                // Play with this number to see what works best
                                {
                                    context.Configuration.AutoDetectChangesEnabled = false;
                                    context.Configuration.ValidateOnSaveEnabled = false;
                                    context.UnclaimedMaturityDatas.AddRange(accounts);
                                    //context.ChangeTracker.DetectChanges();
                                    context.SaveChanges();
                                    context.Dispose();
                                    accounts = new List<UnclaimedMaturityData>();
                                    context = new OLPABDDATAEntities_new();
                                }
                            }

                            context.UnclaimedMaturityDatas.AddRange(accounts);
                            context.ChangeTracker.DetectChanges();
                            context.SaveChanges();
                            context.Dispose();

                            //db.UnclaimedMaturityData.AddRange(dataList);
                            //db.SaveChanges();
                            unclaimedMaturityLog.Status = "SUCCESS";
                            db.Entry(unclaimedMaturityLog).State = EntityState.Modified;
                            db.SaveChanges();
                            reader.Close();
                            //new

                            stopwatch.Stop();
                            elapsed_time = stopwatch.Elapsed.TotalMinutes;
                        }
                        cmd.Dispose();
                        con_excel.Close();
                        con_excel.Dispose();
                    }
                    ViewBag.Message = "SUCCESS" + elapsed_time.ToString();
                }

            }
            catch (Exception ex)
            {
                ViewBag.Message = "ERROR";
            }

            return View();
        }

        #region Log Create
        public static string log_path = ConfigurationManager.AppSettings["LogPath"];
        public static string APPLICATION_ID = "MetLifeBD";
        public static void savelog(String log, bool is_server)
        {

            CreateLogs(log_path, "MetLifeBD", APPLICATION_ID, is_server, log);
        }
        public static void CreateLogs(string log_path, string application_name, string application_id, bool is_server, string log_data)
        {

            String ser_log = "SerLogs";
            String err_log = "ErrorLogs";
            String absolute_path = "";
            try
            {
                String month_date = DateTime.Now.Year.ToString() + DateTime.Now.Date.Month.ToString("d2");
                if (is_server)
                {
                    absolute_path = log_path + month_date + "//" + application_name + "//" + application_id + "//" + ser_log + "//";
                }
                else
                {
                    absolute_path = log_path + "//" + month_date + "//" + application_name + "//" + application_id + "//" + err_log + "//";
                }
                bool exists = Directory.Exists(absolute_path);
                if (!exists)
                    Directory.CreateDirectory(absolute_path);
                FileStream fp = new FileStream(absolute_path + DateTime.Now.Year.ToString() + DateTime.Now.Date.Month.ToString("d2") + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + "_log.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fp);
                sw.WriteLine(DateTime.Now.ToString() + " ==> " + log_data);
                sw.Close();
                fp.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error on save log" + ex.ToString());
            }
        }
        #endregion

        // POST: /Report/Search
        [HttpPost]
        public ActionResult Upload(UploadUnclaimedMaturity reportManager)
        {
            if (!DBContext.hasPermission("Metlife Upload Unclaimed Maturity File"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (reportManager.file != null && reportManager.file.ContentLength > 0)
                {
                    try
                    {
                        string fileName = DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString("d2") + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString() + Util.RandomString(6) + "_" + reportManager.file.FileName;
                        string path = Server.MapPath("~/UploadedFiles/") + fileName;
                        string strFileType = Path.GetExtension(path).ToLower();
                        reportManager.file.SaveAs(path);
                        string a = reportManager.file.FileName;

                        if (strFileType.Trim() != ".xlsx")
                        {
                            ViewBag.Message = "xlsx file required";
                            return View(reportManager);
                        }

                        OleDbConnection con_excel = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 8.0");
                        if (con_excel.State == ConnectionState.Closed)
                            con_excel.Open();

                        OleDbCommand cmd = new OleDbCommand("select count(*) AS DataCount,sum(Amount) FROM [Sheet1$]", con_excel);
                        OleDbDataReader reader = cmd.ExecuteReader();

                        Enc enc = new Enc();

                        if (reader.HasRows)
                        {
                            truncateTable();
                            int count = 0;
                            decimal sum = 0;
                            string bachId = Util.RandomString(20);
                            while (reader.Read())
                            {
                                count = Convert.ToInt32(reader.GetValue(0).ToString());
                                sum = Convert.ToDecimal(reader.GetValue(1).ToString());
                            }
                            UnclaimedMaturityLog unclaimedMaturityLog = new UnclaimedMaturityLog();
                            unclaimedMaturityLog.BachId = bachId.ToString();
                            unclaimedMaturityLog.DataCount = count;
                            savelog("File Upload Data Count" + count, true);
                            unclaimedMaturityLog.UploadTime = DateTime.Now;
                            unclaimedMaturityLog.UploadedBy = Session["userName"].ToString();
                            unclaimedMaturityLog.Status = "processing";
                            unclaimedMaturityLog.FileName = fileName;
                            unclaimedMaturityLog.TotalAmount = sum;
                            db.UnclaimedMaturityLogs.Add(unclaimedMaturityLog);
                            db.SaveChanges();
                            reader.Close();
                        }
                        cmd.Dispose();
                        con_excel.Close();
                        con_excel.Dispose();
                        ViewBag.Message = "File uploaded successfully";
                    }
                    catch (Exception ex)
                    {
                        savelog("File Upload Exception" +ex.ToString(), false);
                        ViewBag.Message = "Exception occured. Please try again later";
                    }
                }
                else
                {
                    ViewBag.Message = "You have not specified a file.";
                }
                return View(reportManager);
            }
        }

        private void truncateTable()
        {
            db.Database.ExecuteSqlCommand("TRUNCATE TABLE [UnclaimedMaturityData]");
        }

    

    }
}