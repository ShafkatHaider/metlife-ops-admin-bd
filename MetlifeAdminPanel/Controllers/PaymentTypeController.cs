﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class PaymentTypeController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /PaymentType/

        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife Payment Type Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View(db.PaymentTypes.ToList());
            }
        }

        //
        // GET: /PaymentType/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Payment Type Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                PaymentType paymenttype = db.PaymentTypes.Find(id);
                if (paymenttype == null)
                {
                    return HttpNotFound();
                }
                return View(paymenttype);
            }
        }

        //
        // GET: /PaymentType/Create

        public ActionResult Create()
        {
            if (!DBContext.hasPermission("Metlife Payment Type Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View();
            }
        }

        //
        // POST: /PaymentType/Create

        [HttpPost]
        public ActionResult Create(PaymentType paymenttype)
        {
            if (!DBContext.hasPermission("Metlife Payment Type Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (paymenttype.Slug != null)
                {
                    paymenttype.Slug = paymenttype.Slug.ToLower();
                }
                paymenttype.Status = 1;
                paymenttype.CreationTime = DateTime.Now;
                if (ModelState.IsValid)
                {
                    db.PaymentTypes.Add(paymenttype);
                    db.SaveChanges();
                    TempData["success"] = "Payment type created successfully";
                    return RedirectToAction("Index");
                }
                else {
                    TempData["failed"] = "Payment type registration failed";

                }
                return View(paymenttype);
            }
        }

        //
        // GET: /PaymentType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Payment Type Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                PaymentType paymenttype = db.PaymentTypes.Find(id);
                if (paymenttype == null)
                {
                    return HttpNotFound();
                }
                return View(paymenttype);
            }
        }

        //
        // POST: /PaymentType/Edit/5

        [HttpPost]
        public ActionResult Edit(PaymentType paymenttype)
        {
            if (!DBContext.hasPermission("Metlife Payment Type Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (paymenttype.Slug != null)
                {
                    paymenttype.Slug = paymenttype.Slug.ToLower();
                }
                if (ModelState.IsValid)
                {
                    db.Entry(paymenttype).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["success"] = "Payment Type updated successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["failed"] = "Payment Type update failed";
                }
                return View(paymenttype);
            }
        }

        //
        // GET: /PaymentType/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Payment Type Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                PaymentType paymenttype = db.PaymentTypes.Find(id);
                if (paymenttype == null)
                {
                    return HttpNotFound();
                }
                return View(paymenttype);
            }
        }

        //
        // POST: /PaymentType/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!DBContext.hasPermission("Metlife Payment Type Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                PaymentType paymenttype = db.PaymentTypes.Find(id);
                db.PaymentTypes.Remove(paymenttype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}