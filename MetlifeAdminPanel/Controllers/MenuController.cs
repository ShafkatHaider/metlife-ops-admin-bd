﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class MenuController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();        
       
        //
        // GET: /Menu/
        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife Menu Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View(db.Menus.ToList());
            }
        }

        //
        // GET: /Menu/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Menu Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Menu menu = db.Menus.Find(id);
                if (menu == null)
                {
                    return HttpNotFound();
                }
                Menu parentMenu = db.Menus.Find(menu.PARENT_MENU);

                Data d = new Data();
                d.menu = menu;
                if (parentMenu != null)
                {
                    d.parentMenu = parentMenu.URL;
                }
                else
                {
                    d.parentMenu = "Top Menu";
                }
                return View(d);
            }
        }

        //
        // GET: /Menu/Create

        List<DisplayTable> list = DBContext.GetMenuList();
               
        public ActionResult Create()
        {
            if (!DBContext.hasPermission("Metlife Menu Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                var menu = new Menu();
                menu.DropDownList = new SelectList(list, "Key", "Display");
                return View(menu);
            }
        }

        //
        // POST: /Menu/Create

        [HttpPost]
        public ActionResult Create(Menu menu)
        {
            if (!DBContext.hasPermission("Metlife Menu Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (menu.PARENT_MENU == null)
                {
                    menu.PARENT_MENU = 0;
                }
                menu.IS_ACTIVE = 1;
                if (ModelState.IsValid)
                {
                    db.Menus.Add(menu);
                    db.SaveChanges();
                    TempData["success"] = "Menu created successfully";
                    return RedirectToAction("Index");
                }
                else {
                    TempData["failed"] = "Menu registration failed";
                }

                return View(menu);
            }
        }

        //
        // GET: /Menu/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Menu Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Menu menu = db.Menus.Find(id);
                if (menu == null)
                {
                    return HttpNotFound();
                }
                menu.DropDownList = new SelectList(list, "Key", "Display");
                return View(menu);
            }
        }

        //
        // POST: /Menu/Edit/5

        [HttpPost]
        public ActionResult Edit(Menu menu)
        {
            if (!DBContext.hasPermission("Metlife Menu Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (menu.PARENT_MENU == null)
                {
                    menu.PARENT_MENU = 0;
                }
                if (ModelState.IsValid)
                {
                    db.Entry(menu).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["success"] = "Menu updated successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["failed"] = "Menu update failed";
                }
                return View(menu);
            }
        }

        //
        // GET: /Menu/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Menu Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Menu menu = db.Menus.Find(id);
                if (menu == null)
                {
                    return HttpNotFound();
                }


                Menu parentMenu = db.Menus.Find(menu.PARENT_MENU);
                Data d = new Data();
                d.menu = menu;
                if (parentMenu != null)
                {
                    d.parentMenu = parentMenu.URL;
                }
                else
                {
                    d.parentMenu = "Top Menu";
                }
                return View(d);
                //return View(menu);
            }
        }

        //
        // POST: /Menu/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!DBContext.hasPermission("Metlife Menu Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Menu menu = db.Menus.Find(id);
                db.Menus.Remove(menu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }



}

