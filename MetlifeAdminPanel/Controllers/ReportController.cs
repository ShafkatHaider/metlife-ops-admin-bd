﻿using MetlifeAdminPanel.Models;
using MetlifeAdminPanel.Services;
using RKLib.ExportData;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class ReportController : Controller
    {
        List<DisplayTable> listPaymentType = DBContext.GetPaymentTypeList();
        List<DisplayTable> listPaymentGateways = DBContext.GetPaymentGateways();
        public static string downloadedFileName = "";
        Enc enc1 = new Enc();

        //for publish
        //string path = "Downloaded/";


        //for test
        string path = AppDomain.CurrentDomain.BaseDirectory + "Downloaded";

        string NewPath = ConfigurationManager.AppSettings["FTP_LOCATION"]; //ConfigurationManager.AppSettings["GenerateToken"].ToString()
        
        // GET: /Report/Search
        public ActionResult Search()
        {
            if (!DBContext.hasPermission("Metlife Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ReportManager reportManager = new ReportManager();
                reportManager.dummySearch.DropDownListForPaymentType = new SelectList(listPaymentType, "Key", "Display");
                reportManager.dummySearch.DropDownListForGateways = new SelectList(listPaymentGateways, "Key", "Display");
                //string id = enc1.Decrypt(reportManager.tranList[0].GatewayTransactionID);


                return View(reportManager);
            }
        }

        // POST: /Report/Search
        [HttpPost]
        public ActionResult Search(ReportManager reportManager)
        {
            if (!DBContext.hasPermission("Metlife Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                List<Transaction> list = DBContext.GetTransactionList(reportManager);
                reportManager.tranList = list;
                reportManager.dummySearch.DropDownListForPaymentType = new SelectList(listPaymentType, "Key", "Display");
                reportManager.dummySearch.DropDownListForGateways = new SelectList(listPaymentGateways, "Key", "Display");
                return View(reportManager);
            }
        }


        // GET: /Report/Search
        public ActionResult Download()
        {
            if (!DBContext.hasPermission("Metlife Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ReportManager reportManager = new ReportManager();
                reportManager.dummySearch.DropDownListForPaymentType = new SelectList(listPaymentType, "Key", "Display");
                reportManager.dummySearch.DropDownListForGateways = new SelectList(listPaymentGateways, "Key", "Display");
                return RedirectToAction("search");
                //return View("search", reportManager);
            }
        }
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        // POST: /Report/Search
        [HttpPost]
        public ActionResult Download(ReportManager reportManager)
        {

            if (!DBContext.hasPermission("Metlife Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Enc enc = new Enc();
                List<Transaction> list = DBContext.GetTransactionList(reportManager);
                reportManager.tranList = list;
                reportManager.dummySearch.DropDownListForPaymentType = new SelectList(listPaymentType, "Key", "Display");
                reportManager.dummySearch.DropDownListForGateways = new SelectList(listPaymentGateways, "Key", "Display");
                List<ExcelData> list2 = new List<ExcelData>();
                foreach (Transaction trn in list.Where(w => w.ExcelDownloaded != 1))
                {
                    ExcelData trn2 = new ExcelData();
                    /**
                        SerialOfDay
                        //GatewayCardNumber
                        TransactionID
                        GatewayRespondTime
                        PaidFor
                        PaidFor
                        PolicyNumber
                        //ReceiptHeading
                        GrandAmount
                        GatewayStatus
                        POName
                        Agent
                        PayerEmail
                        GatewayCardBrand
                        GatewayTransactionID
                     **/
                    if (trn.SerialOfDay != null)
                    {
                        trn2.SerialOfDay = trn.SerialOfDay;
                    }
                    //if (trn.GatewayCardNumber != null)
                    //{
                    //    trn2.GatewayCardNumber = enc.Decrypt(trn.GatewayCardNumber);
                    //}
                    if (trn.RefID != null)
                    {
                        trn2.refID = enc.Decrypt(trn.RefID);
                    }
                    if (trn.CreationTime != null)
                    {
                        trn2.GatewayRespondTime = trn.CreationTime.ToString();
                    }
                    if (trn.PurposeOfPayment != null)
                    {
                        trn2.PaidFor = trn.PurposeOfPayment;
                    }
                    if (trn.PaidFor != null)
                    {
                        trn2.PaidForDetail = trn.PaidFor;
                    }
                    if (trn.PolicyNumber != null)
                    {
                        trn2.PolicyNumber = enc.Decrypt(trn.PolicyNumber);
                    }
                    //if (trn.ReceiptHeading != null)
                    //{
                    //    trn2.ReceiptHeading = trn.ReceiptHeading;
                    //}
                    if (trn.Amount != null)
                    {
                        trn2.GrandAmount = (decimal)trn.Amount;
                    }
                    if (trn.GatewayStatus != null)
                    {
                        trn2.GatewayStatus = trn.GatewayStatus;
                    }
                    if (trn.POName != null)
                    {
                        trn2.POName = trn.POName;
                    }
                    if (trn.Agent != null)
                    {
                        trn2.Agent = trn.Agent;
                    }
                    if (trn.PayerEmail != null)
                    {
                        trn2.PayerEmail = enc.Decrypt(trn.PayerEmail);
                    }
                    if (trn.GatewayCardBrand != null)
                    {
                        trn2.GatewayCardBrand = trn.GatewayCardBrand;
                    }
                    if (trn.GatewayTransactionID != null)
                    {
                        trn2.GatewayTransactionID = enc.Decrypt(trn.GatewayTransactionID);
                    }


                    list2.Add(trn2);
                    Transaction transactionToUpdate = db.Transactions.Where(m => m.ID == trn.ID).Single();
                    if (transactionToUpdate != null)
                    {
                        Update(transactionToUpdate);
                    }
                }

                if (list2.Count != 0)
                {
                    try
                    {
                        DataSet dataSet = ReportManager.ListToDataSet(list2);
                        DataTable dtReport = dataSet.Tables["Table1"].Copy();
                        RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Win");

                        downloadedFileName = "Metlife_Report_" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + "_" + reportManager.dummySearch.gateways + ".xls";
                        //objExport.ExportDetails(dtReport, Export.ExportFormat.Excel, path+downloadedFileName);

                        //string[] columnNames = {"SLNODAY", "ACCTNO", "TRANSACTIONNO", "TRANDATE & TIME", "PAIDFOR", "PAIDFORDETAIL", "POLICYNO", "RECEIPTHEADING", "PAIDAMT", "ACK_SENT", "PONAME", "AGENTCODE", "EMAIL ID","BRAND OF CARD","CBLREFNO"};
                        //int[] a = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14};

                        string[] columnNames = { "SLNO OF DAY", "TRANSACTION NO", "TRANDATE & TIME", "PAID FOR", "PAID FOR DETAIL", "POLICY NO", "PAID AMT", "ACK SENT", "PO NAME", "AGENT CODE", "EMAIL ID", "BRAND OF CARD", "CBL/BKS REF NO" };
                        int[] a = { 0, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 14 };

                        objExport.ExportDetails(dtReport, a, columnNames, Export.ExportFormat.Excel, path + downloadedFileName);


                        savelog("File Upload Starting", true);
                        objExport.ExportDetails(dtReport, a, columnNames, Export.ExportFormat.Excel, NewPath + downloadedFileName);

                        //bool up = FtpUpload(path + downloadedFileName);

                        //send mail
                        savelog("Mail Send Starting", true);
                        SendDownloadEmail();

                        return RedirectToAction("DownloadFile", new { fileName = downloadedFileName, filecount = list2.Count });

                    }
                    catch (Exception Ex)
                    {
                        savelog("File Upload Exception" + Ex.ToString(), false);
                    }

                }
                if (list2.Count == 0)
                {
                    return RedirectToAction("Index", "Message");
                }
                else
                {
                    return View("search", reportManager);
                }

            }
        }


        public ActionResult DownloadFile(string fileName, string filecount)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + fileName);
            ModelState.AddModelError("Success", "Downloaded successfully");

            //SendInvoiceEmail(filecount);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public void SendInvoiceEmail(string filecount)
        {
            try
            {

                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("commin.metlife.com");

                mail.From = new MailAddress("metlifebdolp@metlife.com.bd");
                mail.To.Add("shafkathaider09@gmail.com");
                mail.Subject = "Excel File Downloaded For Successfull Transaction";
                mail.Body = "Total Transaction Downloaded : " + filecount;

                SmtpServer.Port = 25;
                SmtpServer.Credentials = new System.Net.NetworkCredential("username", "password");
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);

            }
            catch (Exception ex)
            {

            }



        }
        public void Update(Transaction trx)
        {
            if (DBContext.hasPermission("Metlife Report"))
            {

                trx.ExcelDownloaded = 1;
                trx.ExcelDownloadTime = DateTime.Now;

                if (ModelState.IsValid)
                {
                    db.Entry(trx).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }

        public bool FtpUpload(string filepath)
        {

            try
            {
                // Get the object used to communicate with the server.
                string ftpLocATION = ConfigurationManager.AppSettings["FTP_LOCATION"];

                savelog("File ftp location" + ftpLocATION, true);

                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpLocATION);
                savelog("ftp request response" + request, true);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                // This example assumes the FTP site uses anonymous logon.
                //request.Credentials = new NetworkCredential("anonymous", "janeDoe@contoso.com");

                // Copy the contents of the file to the request stream.

                byte[] fileContents;
                using (StreamReader sourceStream = new StreamReader(filepath))
                {
                    fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                }

                request.ContentLength = fileContents.Length;

                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(fileContents, 0, fileContents.Length);
                }

                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    savelog("ftp request response true", true);
                    return true;
                }

            }
            catch (Exception e)
            {
                savelog("ftp response exception occured" + e.ToString(), false);
                return false;
            }

        }

        public void SendDownloadEmail()
        {
            try
            {

                //MailMessage mail = new MailMessage();
                //SmtpClient SmtpServer = new SmtpClient("commin.metlife.com");
                //savelog("smtp server  ", true);

                //mail.From = new MailAddress("metlifebdolp@metlife.com.bd");
                //mail.To.Add("Shaila.shahid@metlife.com.bd");
                //mail.To.Add("Monirul.islamact@metlife.com.bd");
                //mail.To.Add("Kazi.Bashar@metlife.com.bd");
                //mail.To.Add("Mohammad.Ali@metlife.com.bd");
                //mail.To.Add("shah-mohammed.nabil@metlife.com.bd");


                //mail.Subject = "Excel File Downloaded For Transaction Successfull";
                //mail.Body = "Online payment transaction report has been downloaded in the common folder";

                //SmtpServer.Port = 25;
                //SmtpServer.Credentials = new System.Net.NetworkCredential("username", "password");       
                //SmtpServer.EnableSsl = true;
                //savelog("smtp server enablessl ", true);
                //SmtpServer.Send(mail);


                SmtpClient client = new SmtpClient("commin.metlife.com", 25);
                savelog("smtp server  " + client, true);

                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress("metlifebdolp@metlife.com.bd");

                    mailMessage.To.Add("Shaila.shahid@metlife.com.bd");
                    mailMessage.To.Add("Monirul.islamact@metlife.com.bd");
                    mailMessage.To.Add("Kazi.Bashar@metlife.com.bd");
                    mailMessage.To.Add("Mohammad.Ali@metlife.com.bd");
                    mailMessage.To.Add("shah-mohammed.nabil@metlife.com.bd");

                    mailMessage.Subject = "Excel File Downloaded For Transaction Successfull";
                    mailMessage.Body = "Online payment transaction report has been downloaded in the common folder";

                    mailMessage.IsBodyHtml = true;
                    savelog("smtp server enablessl ", true);
                    client.Send(mailMessage);
                }

            }
            catch (Exception ex)
            {
                savelog("mail send failed " + ex.ToString(), false);
            }



        }

        #region Log Create
        public static string log_path = ConfigurationManager.AppSettings["LogPath"];
        public static string APPLICATION_ID = "MetLifeBD";
        public static void savelog(String log, bool is_server)
        {

            CreateLogs(log_path, "MetLifeBD", APPLICATION_ID, is_server, log);
        }
        public static void CreateLogs(string log_path, string application_name, string application_id, bool is_server, string log_data)
        {

            String ser_log = "SerLogs";
            String err_log = "ErrorLogs";
            String absolute_path = "";
            try
            {
                String month_date = DateTime.Now.Year.ToString() + DateTime.Now.Date.Month.ToString("d2");
                if (is_server)
                {
                    absolute_path = log_path + month_date + "//" + application_name + "//" + application_id + "//" + ser_log + "//";
                }
                else
                {
                    absolute_path = log_path + "//" + month_date + "//" + application_name + "//" + application_id + "//" + err_log + "//";
                }
                bool exists = Directory.Exists(absolute_path);
                if (!exists)
                    Directory.CreateDirectory(absolute_path);
                FileStream fp = new FileStream(absolute_path + DateTime.Now.Year.ToString() + DateTime.Now.Date.Month.ToString("d2") + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + "_log.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fp);
                sw.WriteLine(DateTime.Now.ToString() + " ==> " + log_data);
                sw.Close();
                fp.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error on save log" + ex.ToString());
            }
        }
        #endregion

    }
}
