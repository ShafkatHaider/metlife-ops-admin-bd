﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class ErrorListController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /ErrorList/

        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife Error Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View(db.ErrorLists.ToList());
            }
        }

        //
        // GET: /ErrorList/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Error Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ErrorList errorlist = db.ErrorLists.Find(id);
                if (errorlist == null)
                {
                    return HttpNotFound();
                }
                return View(errorlist);
            }
        }

        //
        // GET: /ErrorList/Create

        public ActionResult Create()
        {
            if (!DBContext.hasPermission("Metlife Error Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View();
            }
        }

        //
        // POST: /ErrorList/Create

        [HttpPost]
        public ActionResult Create(ErrorList errorlist)
        {
            if (!DBContext.hasPermission("Metlife Error Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                errorlist.CreationTime = DateTime.Now;
                if (ModelState.IsValid)
                {
                    db.ErrorLists.Add(errorlist);
                    db.SaveChanges();
                    TempData["success"] = "Error created successfully";
                    return RedirectToAction("Index");
                }
                else {
                    TempData["failed"] = "Error registration failed";               
                }
                return View(errorlist);
            }
        }

        //
        // GET: /ErrorList/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Error Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ErrorList errorlist = db.ErrorLists.Find(id);
                if (errorlist == null)
                {
                    return HttpNotFound();
                }
                return View(errorlist);
            }
        }

        //
        // POST: /ErrorList/Edit/5

        [HttpPost]
        public ActionResult Edit(ErrorList errorlist)
        {
            if (!DBContext.hasPermission("Metlife Error Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                errorlist.UpdateTime = DateTime.Now;
                if (ModelState.IsValid)
                {
                    db.Entry(errorlist).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["success"] = "Error updated successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["failed"] = "Error update failed";
                }
                return View(errorlist);
            }
        }

        //
        // GET: /ErrorList/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Error Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ErrorList errorlist = db.ErrorLists.Find(id);
                if (errorlist == null)
                {
                    return HttpNotFound();
                }
                return View(errorlist);
            }
        }

        //
        // POST: /ErrorList/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!DBContext.hasPermission("Metlife Error Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ErrorList errorlist = db.ErrorLists.Find(id);
                db.ErrorLists.Remove(errorlist);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}