﻿using MetlifeAdminPanel.Models;
using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MetlifeAdminPanel.Controllers
{
    public class UserController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /User/Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Login login)
        {
            if (ModelState.IsValid)
            {

                if (login.userName == "admin")
                {
                    Enc enc = new Enc();
                    //byte[] pass = enc.Encrypt("100");
                    //string dec = enc.Decrypt(pass);
                    //string a = enc.Decrypt(pass);

                    if (DBContext.IsFirstAdminExist(login.userName, login.password))
                    {
                        string roleName = DBContext.getRoleofUser(login.userName);
                        if (roleName != "")
                        {
                            System.Web.HttpContext.Current.Session["roleName"] = roleName;
                            System.Web.HttpContext.Current.Session["userName"] = login.userName;
                            return RedirectToAction("show", "Dashboard");
                        }
                        
                    }
                }
                else
                {
                    if (loginValidation(login.userName, login.password))
                    {
                        string roleName = DBContext.getRoleofUser(login.userName);
                        if (roleName != "")
                        {
                            System.Web.HttpContext.Current.Session["roleName"] = roleName;
                            System.Web.HttpContext.Current.Session["userName"] = login.userName;
                        }
                        //return RedirectToAction("Action", "Error");
                        return RedirectToAction("show", "Dashboard");
                    }
                    else
                    {
                        ViewBag.failed = "Username or password is incorrect";
                        return View(login);
                    }
                }
            }
            return View(login);
        }



        //
        // GET: /User/
        [HandleError] 
        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife User Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else {
                return View(db.User_Info.ToList());
            }
        }

        //
        // GET: /User/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife User Management2"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                User_Info user_info = db.User_Info.Find(id);
                if (user_info == null)
                {
                    return HttpNotFound();
                }
                return View(user_info);
            }
        }

        //
        // GET: /User/Create

        public ActionResult Create()
        {
            if (!DBContext.hasPermission("Metlife User Management2"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View();
            }
        }

        //
        // POST: /User/Create

        [HttpPost]
        public ActionResult Create(User_Info user_info)
        {
            if (!DBContext.hasPermission("Metlife User Management2"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                user_info.CREATE_TIME = DateTime.Now;

                user_info.STATUS = "Active";
                if (ModelState.IsValid)
                {
                    db.User_Info.Add(user_info);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(user_info);
            }
        }

        //
        // GET: /User/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife User Management2"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                User_Info user_info = db.User_Info.Find(id);
                if (user_info == null)
                {
                    return HttpNotFound();
                }
                return View(user_info);
            }
        }

        //
        // POST: /User/Edit/5

        [HttpPost]
        public ActionResult Edit(User_Info user_info)
        {
            if (!DBContext.hasPermission("Metlife User Management2"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                user_info.UPDATE_TIME = DateTime.Now;
                if (ModelState.IsValid)
                {
                    db.Entry(user_info).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return View(user_info);
            }
        }

        //
        // GET: /User/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife User Management2"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                User_Info user_info = db.User_Info.Find(id);
                if (user_info == null)
                {
                    return HttpNotFound();
                }
                return View(user_info);
            }
        }

        //
        // POST: /User/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!DBContext.hasPermission("Metlife User Management2"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                User_Info user_info = db.User_Info.Find(id);
                db.User_Info.Remove(user_info);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        private bool loginValidation(string user, string pass)
        {

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            string path = (string)settingsReader.GetValue("AD_CONFIG", typeof(String));
             //path = "LDAP://ou=users,ou=bd-dhaka,ou=measa,dc=alico,dc=corp";
            // path = "LDAP://DC=sslwireless,DC=com";
            
            DirectoryEntry de = new DirectoryEntry(path, user, pass, AuthenticationTypes.Secure);
            DirectorySearcher ds = new DirectorySearcher(de);
            try
            {
                ds.FindOne();

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
            return false;
        }

        public static void reloadUser()
        {
            try
            {
                DirectoryEntry directoryEntry = new DirectoryEntry("WinNT://" + Environment.UserDomainName);
                DirectorySearcher ds = new DirectorySearcher(directoryEntry);
                ds.Filter = "(&((&(objectCategory=Person)(objectClass=User)))(samaccountname=" + Environment.UserDomainName + "))";

                string userNames = "";
                string authenticationType = "";
                foreach (DirectoryEntry child in directoryEntry.Children)
                {
                    if (child.SchemaClassName == "User")
                    {
                        userNames += child.Name + Environment.NewLine; //Iterates and binds all user using a newline
                        authenticationType += child.Username + Environment.NewLine;
                    }
                }
                //Console.WriteLine(userNames);
                //Console.WriteLine(authenticationType);
            }
            catch (Exception)
            {
                Console.WriteLine("Network error occured.");
            }
        }

        public ActionResult Logout()
        {
            System.Web.HttpContext.Current.Session.Clear();
            System.Web.HttpContext.Current.Session.Abandon();
            return RedirectToAction("Login");
        }


    }
}