﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult AccessDenied()
        {
            return View();
        }

        public ActionResult NotFound()
        {
           return RedirectToAction("Action", "Error");           
        }

        public ActionResult PermissionNeeded()
        {
            return View();
        }


        public ActionResult Action() {
            if (System.Web.HttpContext.Current.Session["userName"] != null)
            {
                return RedirectToAction("PermissionNeeded", "Error");
            }
            else
            {
                return RedirectToAction("AccessDenied", "Error");
            }
        }
    }
}
