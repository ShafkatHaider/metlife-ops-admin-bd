﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RKLib.ExportData;

namespace MetlifeAdminPanel.Controllers
{
    public class UnclaimedSubmissionController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
        public static string downloadedFileName = "";

        //for publish
        //string path = "Downloaded/Submission/";

        //for test
        string path = AppDomain.CurrentDomain.BaseDirectory + "/Downloaded/Submission/";
        // GET: /Report/Search
        public ActionResult Search()
        {
            if (!DBContext.hasPermission("Metlife Unclaimed Submission Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ReportManager.SubmissionReportManager reportManager = new ReportManager.SubmissionReportManager();
                return View(reportManager);
            }
        }

        // POST: /Report/Search
        [HttpPost]
        public ActionResult Search(ReportManager.SubmissionReportManager reportManager)
        {
            if (!DBContext.hasPermission("Metlife Unclaimed Submission Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                List<UnclaimedSubmission> list = DBContext.GetUnclaimedSubmissionList(reportManager);
                reportManager.tranList = list;
                return View(reportManager);
            }
        }


        // GET: /Report/Search
        public ActionResult Download()
        {
            if (!DBContext.hasPermission("Metlife Unclaimed Submission Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ReportManager.SubmissionReportManager reportManager = new ReportManager.SubmissionReportManager();
                return RedirectToAction("search");
            }
        }

        // POST: /Report/Search
        [HttpPost]
        public ActionResult Download(ReportManager.SubmissionReportManager reportManager)
        {
            if (!DBContext.hasPermission("Metlife Unclaimed Submission Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Enc enc = new Enc();
                List<UnclaimedSubmission> list = DBContext.GetUnclaimedSubmissionList(reportManager);
                reportManager.tranList = list;
                List<ExcelDataSubmission> list2 = new List<ExcelDataSubmission>();
                foreach (UnclaimedSubmission trn in list)
                {
                    ExcelDataSubmission trn2 = new ExcelDataSubmission();                    

                    if (trn.PolicyNumber != null)
                    {
                        trn2.PolicyNumber = enc.Decrypt(trn.PolicyNumber);
                    }

                    if (trn.POName != null)
                    {
                        trn2.POName = enc.Decrypt(trn.POName);
                    }

                    if (trn.YearOfBirth != null)
                    {
                        trn2.YearOfBirth = enc.Decrypt(trn.YearOfBirth);
                    }

                    if (trn.Email != null)
                    {
                        trn2.Email = enc.Decrypt(trn.Email);
                    }

                    if (trn.Phone != null)
                    {
                        trn2.Phone = enc.Decrypt(trn.Phone);
                    }

                    if (trn.BenefitType != null)
                    {
                        trn2.BenefitType = trn.BenefitType;
                    }
                    if (trn.CreationTime != null)
                    {
                        trn2.CreationTime = String.Format("{0:dd-MMM-yyyy HH:mm:ss}", trn.CreationTime);
                    }
                    list2.Add(trn2);
                    UnclaimedSubmission transactionToUpdate = db.UnclaimedSubmissions.Where(m => m.ID == trn.ID).Single();
                    if (transactionToUpdate != null)
                    {
                        Update(transactionToUpdate);
                    }
                }
                try
                {
                    DataSet dataSet = ReportManager.ListToDataSet(list2);
                    DataTable dtReport = dataSet.Tables["Table1"].Copy();
                    RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Win");
                    downloadedFileName = "Metlife_Unclaimed_Submission_Report_" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + ".xls";
                    string[] columnNames = {"Policy Number", "Year Of Birth", "Policy Owner Name", "Email", "Phone", "Benefit Type", "Creation Time"};
                    int[] a = { 0, 1,  2, 3, 4, 5,6};
                    objExport.ExportDetails(dtReport, a, columnNames, Export.ExportFormat.Excel, path + downloadedFileName);
                    return RedirectToAction("DownloadFile", new { fileName = downloadedFileName });
                }
                catch (Exception Ex)
                {

                }
                return View("search", reportManager);
            }
        }


        public ActionResult DownloadFile(string fileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + fileName);
            ModelState.AddModelError("Success", "Downloaded successfully");
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }


        public void Update(UnclaimedSubmission trx)
        {
            if (DBContext.hasPermission("Metlife Unclaimed Submission Report"))
            {
                trx.Downloaded = 1;
                if (ModelState.IsValid)
                {
                    db.Entry(trx).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        
    }
}