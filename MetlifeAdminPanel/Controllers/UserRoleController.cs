﻿using MetlifeAdminPanel.Models;
using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class UserRoleController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();


        // GET: /UserRole/
        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife User Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                var user_role = db.User_Role.Include(u => u.Role).Include(u => u.User_Info);
                return View(user_role.ToList());
            }
        }

        //
        // GET: /UserRole/Details/5
        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife User Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                User_Role user_role = db.User_Role.Find(id);
                if (user_role == null)
                {
                    return HttpNotFound();
                }
                return View(user_role);
            }
        }

        //
        // GET: /UserRole/Create
        List<DisplayTable> listUser = DBContext.GetUserList();
        List<DisplayTable> listRole = DBContext.GetRoleList();



        public ActionResult Create()
        {
            if (!DBContext.hasPermission("Metlife User Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                var userRole = new DummyUserRole();
                //userRole.DropDownListForUser = new SelectList(listUser, "Key", "Display");
                userRole.DropDownListForRole = new SelectList(listRole, "Key", "Display");
                //ViewBag.ROLE_ID = new SelectList(db.Roles, "ID", "NAME");
                //ViewBag.USER_ID = new SelectList(db.User_Info, "ID", "USERID");
                return View(userRole);
            }
        }

        //
        // POST: /UserRole/Create

        [HttpPost]
        public ActionResult Create(DummyUserRole user_role)
        {
            if (!DBContext.hasPermission("Metlife User Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (user_role.userName != null && user_role.ROLE_ID != null)
                {

                    var users = db.User_Info.Where(b => b.NAME == user_role.userName).ToList();
                    foreach (User_Info user in users)
                    {

                        db.User_Info.Remove(user);
                    }

                    User_Info userInfo = new User_Info();
                    userInfo.NAME = user_role.userName;
                    userInfo.USERID = "";
                    userInfo.STATUS = "Acive";
                    userInfo.CREATE_TIME = DateTime.Now;
                    db.User_Info.Add(userInfo);
                    db.SaveChanges();

                    if (ModelState.IsValid)
                    {

                        foreach (User_Info user in users)
                        {
                            var userRoles = db.User_Role.Include(r => r.User_Info).Include(r => r.Role).Where(b => b.USER_ID == user.ID).ToList();
                            foreach (User_Role userRole1 in userRoles)
                            {
                                db.User_Role.Remove(userRole1);
                            }
                        }
                        db.SaveChanges();

                        User_Role userRole = new User_Role();
                        userRole.USER_ID = userInfo.ID;
                        userRole.ROLE_ID = user_role.ROLE_ID;

                        db.User_Role.Add(userRole);
                        db.SaveChanges();
                        TempData["success"] = "Role Assigned successfully";
                        return RedirectToAction("Index");
                    }
                    else {
                        TempData["failed"] = "Role Assign failed";
                    }
                }

                var userRole2 = new User_Role();
                userRole2.DropDownListForUser = new SelectList(listUser, "Key", "Display");
                userRole2.DropDownListForRole = new SelectList(listRole, "Key", "Display");
                return View(userRole2);
            }
        }

        /*
        [HttpPost]
        public ActionResult Create(User_Role user_role)
        {
            if (user_role.USER_ID != null && user_role.ROLE_ID != null)
            {
                if (ModelState.IsValid)
                {
                    var userRoles = db.User_Role.Include(r => r.User_Info).Include(r => r.Role)
                                        .Where(b => b.USER_ID == user_role.USER_ID).ToList();
                    foreach (User_Role userRole1 in userRoles)
                    {
                        db.User_Role.Remove(userRole1);
                    }
                    db.SaveChanges();
                    db.User_Role.Add(user_role);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            var userRole = new User_Role();
            userRole.DropDownListForUser = new SelectList(listUser, "Key", "Display");
            userRole.DropDownListForRole = new SelectList(listRole, "Key", "Display");
            return View(userRole);
        }*/

        //
        // GET: /UserRole/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife User Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                User_Role user_role = db.User_Role.Find(id);
                if (user_role == null)
                {
                    return HttpNotFound();
                }
                //user_role.DropDownListForUser = new SelectList(listUser, "Key", "Display");
                //user_role.DropDownListForRole = new SelectList(listRole, "Key", "Display");

                ViewBag.ROLE_ID = new SelectList(db.Roles, "ID", "NAME", user_role.ROLE_ID);
                ViewBag.USER_ID = new SelectList(db.User_Info, "ID", "NAME", user_role.USER_ID);
                return View(user_role);
            }
        }

        //
        // POST: /UserRole/Edit/5

        [HttpPost]
        public ActionResult Edit(User_Role user_role)
        {
            if (!DBContext.hasPermission("Metlife User Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (user_role.USER_ID != null && user_role.ROLE_ID != null)
                {
                    if (ModelState.IsValid)
                    {
                        db.Entry(user_role).State = EntityState.Modified;
                        db.SaveChanges();
                        return RedirectToAction("Index");
                    }
                }
                ViewBag.ROLE_ID = new SelectList(db.Roles, "ID", "NAME", user_role.ROLE_ID);
                ViewBag.USER_ID = new SelectList(db.User_Info, "ID", "NAME", user_role.USER_ID);
                return View(user_role);
            }
        }

        //
        // GET: /UserRole/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife User Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                User_Role user_role = db.User_Role.Find(id);
                if (user_role == null)
                {
                    return HttpNotFound();
                }
                return View(user_role);
            }
        }

        //
        // POST: /UserRole/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!DBContext.hasPermission("Metlife User Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                User_Role user_role = db.User_Role.Find(id);
                db.User_Role.Remove(user_role);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}