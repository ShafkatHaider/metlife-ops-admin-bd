﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class EmailNotificationsController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /EmailNotifications/

        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife Email Notifications"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View(db.EmailNotifications.OrderByDescending(data => data.Id).ToList());
            }
        }

        //
        // GET: /EmailNotifications/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Email Notifications"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                EmailNotification emailnotifications = db.EmailNotifications.Find(id);
                if (emailnotifications == null)
                {
                    return HttpNotFound();
                }
                return View(emailnotifications);
            }
        }

        
    }
}