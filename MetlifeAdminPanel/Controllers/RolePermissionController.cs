﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class RolePermissionController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /RolePermission/

        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife Role Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                var role_permission = db.Role_Permission.Include(r => r.Permission).Include(r => r.Role);
                return View(role_permission.ToList());
            }
        }

        //
        // GET: /RolePermission/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Role Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Role_Permission role_permission = db.Role_Permission.Find(id);
                if (role_permission == null)
                {
                    return HttpNotFound();
                }
                return View(role_permission);
            }
        }


        List<DisplayTable> listPermission = DBContext.GetPermissionList();
        List<DisplayTable> listRole = DBContext.GetRoleList();
        //
        // GET: /RolePermission/Create
        public ActionResult Create()
        {
            if (!DBContext.hasPermission("Metlife Role Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                var rolePermission = new Role_Permission();
                rolePermission.DropDownListForPermission = DBContext.GetpermissionSelectList(1);
                rolePermission.DropDownListForRole = new SelectList(listRole, "Key", "Display");
                //ViewBag.PERMISSION_ID = new SelectList(db.Permissions, "ID", "NAME");
                //ViewBag.ROLE_ID = new SelectList(db.Roles, "ID", "NAME");
                return View(rolePermission);
            }
        }


        //
        // POST: /RolePermission/Search
        [HttpPost]
        public ActionResult SearchResult(Role_Permission role_permission)
        {
            if (!DBContext.hasPermission("Metlife Role Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                DataObjectsForRolePermission data = new DataObjectsForRolePermission();
                if (role_permission.ROLE_ID != null)
                {
                    var rolePermission = new Role_Permission();
                    int roleId = (int)role_permission.ROLE_ID;
                    rolePermission.DropDownListForPermission = DBContext.GetpermissionSelectList(roleId);
                    rolePermission.DropDownListForRole = new SelectList(listRole, "Key", "Display");

                    Role role = db.Roles.Find(role_permission.ROLE_ID);

                    data.rolePermission = rolePermission;
                    data.role = role;
                    return View(data);
                }

                return RedirectToAction("Create");
            }
        }


        //
        // POST: /RolePermission/Create

        [HttpPost]
        public ActionResult Create(Role_Permission rolePermission)
        {
            if (!DBContext.hasPermission("Metlife Role Permission Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                //if (data.rolePermission != null && data.role != null){
                if (rolePermission.SelectedValues != null && rolePermission.ROLE_ID != null)
                {
                    if (ModelState.IsValid)
                    {
                        var role_permissions = db.Role_Permission.Include(r => r.Permission).Include(r => r.Role)
                            .Where(b => b.ROLE_ID == rolePermission.ROLE_ID).ToList();

                        foreach (Role_Permission role_permission1 in role_permissions)
                        {
                            db.Role_Permission.Remove(role_permission1);
                        }
                        db.SaveChanges();

                        foreach (int permissionId in rolePermission.SelectedValues)
                        {
                            rolePermission.PERMISSION_ID = permissionId;
                            db.Role_Permission.Add(rolePermission);
                            db.SaveChanges();
                            TempData["success"] = "Permission Assigned successfully";

                        }
                        return RedirectToAction("Index");
                    }
                    else {
                        TempData["failed"] = "Permission Assign failed";
                    }
                }
                //}
                ViewBag.PERMISSION_ID = new SelectList(db.Permissions, "ID", "NAME", rolePermission.PERMISSION_ID);
                ViewBag.ROLE_ID = new SelectList(db.Roles, "ID", "NAME", rolePermission.ROLE_ID);
                return View(rolePermission);
            }
        }

        
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}