﻿using MetlifeAdminPanel.Services;
using RKLib.ExportData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class ECollectionController : Controller
    {
        List<DisplayTable> listPayingBank = DBContext.GetPayingBankList();
        public static string downloadedFileName = "";
        
        //for publish
        //string path = "Downloaded/Ecollection/";

        //for test
        string path = AppDomain.CurrentDomain.BaseDirectory + "/Downloaded/Ecollection/";
        
        // GET: /ECollection Report/Search
        public ActionResult Search()
        {
            if (!DBContext.hasPermission("Metlife E-Collection Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                EcollectionManager reportManager = new EcollectionManager();
                reportManager.dummySearch.DropDownListForECollectionBank = new SelectList(listPayingBank, "Key", "Display");
                return View(reportManager);
            }
        }

        // POST: /Report/Search
        [HttpPost]
        public ActionResult Search(EcollectionManager reportManager)
        {
            if (!DBContext.hasPermission("Metlife E-Collection Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                List<ECollection> list = DBContext.GetEcollectionList(reportManager);
                reportManager.tranList = list;
                reportManager.dummySearch.DropDownListForECollectionBank = new SelectList(listPayingBank, "Key", "Display");

                return View(reportManager);
            }
        }


        // GET: /Report/Search
        public ActionResult Download()
        {
            if (!DBContext.hasPermission("Metlife E-Collection Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                EcollectionManager reportManager = new EcollectionManager();
                reportManager.dummySearch.DropDownListForECollectionBank = new SelectList(listPayingBank, "Key", "Display");
                return RedirectToAction("search");
                //return View("search", reportManager);
            }
        }
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        // POST: /Report/Search
        [HttpPost]
        public ActionResult Download(EcollectionManager reportManager)
        {

            if (!DBContext.hasPermission("Metlife E-Collection Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                List<ECollection> list = DBContext.GetEcollectionList(reportManager);
                reportManager.tranList = list;
                reportManager.dummySearch.DropDownListForECollectionBank = new SelectList(listPayingBank, "Key", "Display");
                List<ExcelDatForEcollection> list2 = new List<ExcelDatForEcollection>();
                foreach (ECollection trn in list)
                {
                    ExcelDatForEcollection trn2 = new ExcelDatForEcollection();
                    trn2.Orderid = trn.OrderID;
                    trn2.beneficiaryName = trn.BeneficiaryName;
                    trn2.bankAccountNumber = trn.BankAccountNumber;
                    trn2.payingBank = DBContext.GetEcollectionBankById(trn.PayingBank).Replace("\r\n", string.Empty).ToString();

                    trn2.bankName = trn.BankName;
                    trn2.branch = trn.Branch;
                    trn2.Amount = trn.Amount;
                    list2.Add(trn2);
                }
                try
                {
                    DataSet dataSet = ReportManager.ListToDataSet(list2);
                    DataTable dtReport = dataSet.Tables["Table1"].Copy();
                    RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Win");

                    downloadedFileName = "Metlife_Ecollection_Report_" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + ".xls";
                    objExport.ExportDetails(dtReport, Export.ExportFormat.Excel, path + downloadedFileName);
                    return RedirectToAction("DownloadFile", new { fileName = downloadedFileName });

                }
                catch (Exception Ex)
                {
                }
                return View("search", reportManager);
            }
        }


        public ActionResult DownloadFile(string fileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + fileName);
            ModelState.AddModelError("Success", "Downloaded successfully");
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }


    }
}
