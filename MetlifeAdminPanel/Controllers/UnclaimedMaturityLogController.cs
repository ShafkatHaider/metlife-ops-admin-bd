﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class UnclaimedMaturityLogController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

       

        public ActionResult Index()
        {

            if (!DBContext.hasPermission("Metlife Upload Unclaimed Maturity Log"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View(db.UnclaimedMaturityLogs.ToList());
            }
        }

        
    }
}