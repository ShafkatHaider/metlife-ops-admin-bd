﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class SwitchingLogsController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /SwitchingLogs/

        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife Switching Logs"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View(db.SwitchingLogs.ToList());
            }
        }

        //
        // GET: /SwitchingLogs/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Switching Logs"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                SwitchingLog switchinglog = db.SwitchingLogs.Find(id);
                if (switchinglog == null)
                {
                    return HttpNotFound();
                }
                return View(switchinglog);
            }
        }

        
    }
}