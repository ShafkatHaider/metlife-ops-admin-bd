﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class GatewayController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /Gateway/

        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife Gateway Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {

                return View(db.PaymentGateways.ToList());
            }
        }

        //
        // GET: /Gateway/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Gateway Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                PaymentGateway paymentgateway = db.PaymentGateways.Find(id);
                if (paymentgateway == null)
                {
                    return HttpNotFound();
                }
                return View(paymentgateway);
            }
        }

        //
        // GET: /Gateway/Create

        public ActionResult Create()
        {
            if (!DBContext.hasPermission("Metlife Gateway Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View();
            }
        }

        //
        // POST: /Gateway/Create

        [HttpPost]
        public ActionResult Create(PaymentGateway paymentgateway)
        {
            if (!DBContext.hasPermission("Metlife Gateway Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                paymentgateway.IsEnabled = 1;
                paymentgateway.CreatedAt = DateTime.Now;

                if (ModelState.IsValid)
                {
                    if (paymentgateway.GatewayFeeType != "")
                    {
                        if (paymentgateway.GatewayFeeType == "slab")
                        {
                            if (paymentgateway.SlabRates != null)
                            {
                                string pattern = @"^[0-9]*[-][0-9]*[=][0-9]+(,[0-9]*[-][0-9]*[=][0-9]*)*$";
                                Match match = Regex.Match(paymentgateway.SlabRates, pattern);
                                if (!match.Success)
                                {
                                    TempData["failed"] = "Slab rate pattern is incorrect";
                                    return View(paymentgateway);
                                }
                            }
                            else
                            {
                                TempData["failed"] = "Slab rate can not be null";
                                return View(paymentgateway);
                            }
                        }
                        if (paymentgateway.GatewayFeeType != "slab")
                        {
                            if (paymentgateway.GatewayFeeValue == null)
                            {
                                TempData["failed"] = "Gateway Fee Value can not be null";
                                return View(paymentgateway);
                            }
                        }
                    }
                    else {
                        TempData["failed"] = "Please select Gateway fee type";
                        return View(paymentgateway);
                    }
                    

                    db.PaymentGateways.Add(paymentgateway);
                    db.SaveChanges();
                    TempData["success"] = "Gateway created successfully";
                    return RedirectToAction("Index");
                }
                else {
                    TempData["failed"] = "Gateway registration failed";
                }

                return View(paymentgateway);
            }
        }

        //
        // GET: /Gateway/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Gateway Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                PaymentGateway paymentgateway = db.PaymentGateways.Find(id);
                if (paymentgateway == null)
                {
                    return HttpNotFound();
                }
                return View(paymentgateway);
            }
        }

        //
        // POST: /Gateway/Edit/5

        [HttpPost]
        public ActionResult Edit(PaymentGateway paymentgateway)
        {
            if (!DBContext.hasPermission("Metlife Gateway Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                paymentgateway.UpdatedAt = DateTime.Now;
                if (ModelState.IsValid)
                {
                    db.Entry(paymentgateway).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["success"] = "Gateway Updated successfully";
                    return RedirectToAction("Index");
                }
                else {
                    TempData["failed"] = "Gateway Update failed";
                }
                return View(paymentgateway);
            }
        }

        //
        // GET: /Gateway/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Gateway Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                PaymentGateway paymentgateway = db.PaymentGateways.Find(id);
                if (paymentgateway == null)
                {
                    return HttpNotFound();
                }
                return View(paymentgateway);
            }
        }

        //
        // POST: /Gateway/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!DBContext.hasPermission("Metlife Gateway Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                PaymentGateway paymentgateway = db.PaymentGateways.Find(id);
                db.PaymentGateways.Remove(paymentgateway);
                db.SaveChanges();
                TempData["success"] = "Gateway Deleted successfully";
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}