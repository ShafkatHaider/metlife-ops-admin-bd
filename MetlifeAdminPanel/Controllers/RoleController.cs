﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class RoleController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /Role/
        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View(db.Roles.ToList());
            }
        }

        //
        // GET: /Role/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Role role = db.Roles.Find(id);
                if (role == null)
                {
                    return HttpNotFound();
                }
                return View(role);
            }
        }

        //
        // GET: /Role/Create

        public ActionResult Create()
        {
            if (!DBContext.hasPermission("Metlife Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View();
            }
        }

        //
        // POST: /Role/Create

        [HttpPost]
        public ActionResult Create(Role role)
        {
            if (!DBContext.hasPermission("Metlife Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                role.IS_ACTIVE = 1;
                if (ModelState.IsValid)
                {
                    db.Roles.Add(role);
                    db.SaveChanges();
                    TempData["success"] = "Role created successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["failed"] = "Role registration failed";
                }

                return View(role);
            }
        }

        //
        // GET: /Role/Edit/5
        public ActionResult Edit(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Role role = db.Roles.Find(id);
                if (role == null)
                {
                    return HttpNotFound();
                }
                return View(role);
            }
        }

        //
        // POST: /Role/Edit/5

        [HttpPost]
        public ActionResult Edit(Role role)
        {
            if (!DBContext.hasPermission("Metlife Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    db.Entry(role).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["success"] = "Role updated successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["failed"] = "Role update failed";
                }
                return View(role);
            }
        }

        //
        // GET: /Role/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Role role = db.Roles.Find(id);
                if (role == null)
                {
                    return HttpNotFound();
                }
                return View(role);
            }
        }

        //
        // POST: /Role/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!DBContext.hasPermission("Metlife Role Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Role role = db.Roles.Find(id);
                db.Roles.Remove(role);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}