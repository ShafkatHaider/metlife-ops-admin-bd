﻿using MetlifeAdminPanel.Models;
using MetlifeAdminPanel.Services;
using RKLib.ExportData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class D2CReportingController : Controller
    {
        List<DisplayTable> listPaymentType = DBContext.GetPaymentTypeList();
        List<DisplayTable> listPaymentGateways = DBContext.GetPaymentGateways();
        public static string downloadedFileName = "";
        Enc enc1 = new Enc();
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();


        string path = AppDomain.CurrentDomain.BaseDirectory + "Downloaded";
        string NewPath = ConfigurationManager.AppSettings["FTP_LOCATION_D2C"]; 

        public ActionResult Search()
        {
            if (!DBContext.hasPermission("MetLife D2C Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ReportManagerD2C reportManager = new ReportManagerD2C();
                reportManager.dummySearch.DropDownListForPaymentType = new SelectList(listPaymentType, "Key", "Display");
                reportManager.dummySearch.DropDownListForGateways = new SelectList(listPaymentGateways, "Key", "Display");

                return View(reportManager);
            }
        }

        [HttpPost]
        public ActionResult Search(ReportManagerD2C reportManager)
        {
            if (!DBContext.hasPermission("MetLife D2C Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                List<D2C_Transaction_Log> list = DBContext.GetD2CLog(reportManager);
                reportManager.tranList = list;
                reportManager.dummySearch.DropDownListForPaymentType = new SelectList(listPaymentType, "Key", "Display");
                reportManager.dummySearch.DropDownListForGateways = new SelectList(listPaymentGateways, "Key", "Display");
                return View(reportManager);
            }
        }

        public ActionResult DownloadReport()
        {
            if (!DBContext.hasPermission("MetLife D2C Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ReportManagerD2C reportManager = new ReportManagerD2C();
                reportManager.dummySearch.DropDownListForPaymentType = new SelectList(listPaymentType, "Key", "Display");
                reportManager.dummySearch.DropDownListForGateways = new SelectList(listPaymentGateways, "Key", "Display");

                return RedirectToAction("search");
                //return View("search", reportManager);
            }
        }
        [HttpPost]
        public ActionResult DownloadReport(ReportManagerD2C reportManager)
        {

            if (!DBContext.hasPermission("MetLife D2C Report"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                Enc enc = new Enc();
                List<D2C_Transaction_Log> list = DBContext.GetD2CLog(reportManager);
                reportManager.tranList = list;
                reportManager.dummySearch.DropDownListForPaymentType = new SelectList(listPaymentType, "Key", "Display");
                reportManager.dummySearch.DropDownListForGateways = new SelectList(listPaymentGateways, "Key", "Display");
                List<ExcelDataD2C> list2 = new List<ExcelDataD2C>();
                foreach (D2C_Transaction_Log trn in list.Where(w => w.ExcelDownload != 1))
                {
                    ExcelDataD2C trn2 = new ExcelDataD2C();
                    //"SLNO OF DAY", "TRANSACTION NO", "TRANDATE & TIME", "PAID FOR",
                    //    "PAID FOR DETAIL", "POLICY NO", "PAID AMT", "ACK SENT", "PO NAME", 
                    //    "AGENT CODE", "EMAIL ID", "BRAND OF CARD", "CBL/BKS REF NO", "CLIENT NAME"

                    if (trn.SerialOfDay != null)
                    {
                        trn2.SerialOfDay = trn.SerialOfDay;
                    }
                    if (trn.TransactionNo != null)
                    {
                        trn2.TransactionNo = trn.TransactionNo.ToString();
                    }
                    if (trn.PaymentDate != null)
                    {
                        trn2.PaymentDate = trn.PaymentDate.ToString();
                    }
                    if (trn.PaidFor != null)
                    {
                        trn2.PaidFor = trn.PaidFor;
                    }
                    if (trn.ApplicationType != null)
                    {
                        trn2.ApplicationType = trn.ApplicationType;
                    }
                    
                    if (trn.PolicyNumber != null)
                    {
                        trn2.PolicyNumber = trn.PolicyNumber;
                    }
                    if (trn.Amount != null)
                    {
                        trn2.Amount = trn.Amount;
                    }
                    if (trn.Status != null)
                    {
                        trn2.Status = trn.Status;
                    }
                    if (trn.POName != null)
                    {
                        trn2.POName = trn.POName;
                    }
                    if (trn.AgentCode != null)
                    {
                        trn2.AgentCode = trn.AgentCode;
                    }
                    if (trn.PayerEmail != null)
                    {
                        trn2.PayerEmail = trn.PayerEmail;
                    }
                    if (trn.BOCard != null)
                    {
                        trn2.BOCard = trn.BOCard;
                    }
                    if (trn.GatewayTransactionID != null)
                    {
                        trn2.GatewayTransactionID = trn.GatewayTransactionID;
                    }
                    if (trn.ClientName != null)
                    {
                        trn2.ClientName = trn.ClientName;
                    }
                    list2.Add(trn2);
                    D2C_Transaction_Log transactionToUpdate = db.D2C_Transaction_Log.Where(m => m.Id == trn.Id).Single();
                    if (transactionToUpdate != null)
                    {
                        Update(transactionToUpdate);
                    }
                }

                if (list2.Count != 0)
                {
                    try
                    {
                        DataSet dataSet = ReportManager.ListToDataSet(list2);
                        DataTable dtReport = dataSet.Tables["Table1"].Copy();
                        RKLib.ExportData.Export objExport = new RKLib.ExportData.Export("Win");

                        downloadedFileName = "Metlife_D2C_Log_Report_" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + DateTime.Now.Millisecond + "_" + reportManager.dummySearch.gateways + ".xls";

                        //string[] columnNames = { "Application Type", "Policy Number", "Amount", "Grand Amount", "Payer Email", "Payment Date", "Status", "Gateway TransactionID", "PO NAME", "PO Mobile", "ClientName" };
                        //int[] a = { 0, 1,2, 3, 4, 5, 6, 8, 9, 10 };


                        //string[] columnNames = { "Application Type", "Policy Number", "Amount", "Grand Amount", "Payer Email", "Payment Date", "Status", "Gateway TransactionID", "PO NAME", "PO Mobile", "ClientName" };
                        //int[] a = { 0,1,2,3,4,5,6,7,8,9,10};

                        string[] columnNames = { "SLNO OF DAY", "TRANSACTION NO", "TRANDATE & TIME", "PAID FOR", "PAID FOR DETAIL", "POLICY NO", "PAID AMT", "ACK SENT", "PO NAME", "AGENT CODE", "EMAIL ID", "BRAND OF CARD", "CBL/BKS REF NO", "CLIENT NAME" };
                        int[] a = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

                        objExport.ExportDetails(dtReport, a, columnNames, Export.ExportFormat.Excel, path + downloadedFileName);


                       
                        objExport.ExportDetails(dtReport, a, columnNames, Export.ExportFormat.Excel, NewPath + downloadedFileName);

                        savelog("Mail Send Starting", true);
                        SendDownloadEmail();
                        return RedirectToAction("DownloadFile", new { fileName = downloadedFileName, filecount = list2.Count });

                    }
                    catch (Exception Ex)
                    {
                      
                    }

                }
                if (list2.Count == 0)
                {
                    return RedirectToAction("Index", "Message");
                }
                else
                {
                    return View("search", reportManager);
                }

            }
        }

        public ActionResult DownloadFile(string fileName, string filecount)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(path + fileName);
            ModelState.AddModelError("Success", "Downloaded successfully");

            //SendInvoiceEmail(filecount);

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public void Update(D2C_Transaction_Log trx)
        {
            if (DBContext.hasPermission("MetLife D2C Report"))
            {

                trx.ExcelDownload = 1;
              
                if (ModelState.IsValid)
                {
                    db.Entry(trx).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        public EmailSetting GetEmailInformation()
        {
            EmailSetting emailSettings = new EmailSetting();

            using (var db = new OLPABDDATAEntities_new())
            {
                emailSettings = db.EmailSettings.Where(x=>x.Username=="D2C").FirstOrDefault();

                return emailSettings;

            }
        }

        public void SendDownloadEmail()
        {
            try
            {

                EmailSetting emailSettings = new EmailSetting();
                emailSettings = GetEmailInformation();

                SmtpClient client = new SmtpClient(emailSettings.MailServer, Convert.ToInt32(emailSettings.Port));
                savelog("smtp server  " + client, true);

                using (MailMessage mailMessage = new MailMessage())
                {
                    mailMessage.From = new MailAddress(emailSettings.FromEmail);

                    //mailMessage.To.Add("Shaila.shahid@metlife.com.bd");
                    //mailMessage.To.Add("Monirul.islamact@metlife.com.bd");
                    //mailMessage.To.Add("Kazi.Bashar@metlife.com.bd");
                    //mailMessage.To.Add("Mohammad.Ali@metlife.com.bd");
                    //mailMessage.To.Add("shah-mohammed.nabil@metlife.com.bd");

                    string[] Multi = emailSettings.ToEmail.Split(','); //spiliting input Email id string with comma(,)
                    foreach (string Multiemailid in Multi)
                    {
                        mailMessage.To.Add(new MailAddress(Multiemailid)); //adding multi reciver's Email Id
                    }

                    mailMessage.Subject = "Excel File Downloaded For Transaction Successfull";
                    mailMessage.Body = "Online payment transaction report has been downloaded in the common folder";

                    mailMessage.IsBodyHtml = true;
                    savelog("smtp server enablessl ", true);
                    client.Send(mailMessage);
                }

            }
            catch (Exception ex)
            {
                savelog("mail send failed " + ex.ToString(), false);
            }



        }
        #region Log Create
        public static string log_path = ConfigurationManager.AppSettings["LogPath"];
        public static string APPLICATION_ID = "MetLifeBD";
        public static void savelog(String log, bool is_server)
        {

            CreateLogs(log_path, "MetLifeBD", APPLICATION_ID, is_server, log);
        }
        public static void CreateLogs(string log_path, string application_name, string application_id, bool is_server, string log_data)
        {

            String ser_log = "SerLogs";
            String err_log = "ErrorLogs";
            String absolute_path = "";
            try
            {
                String month_date = DateTime.Now.Year.ToString() + DateTime.Now.Date.Month.ToString("d2");
                if (is_server)
                {
                    absolute_path = log_path + month_date + "//" + application_name + "//" + application_id + "//" + ser_log + "//";
                }
                else
                {
                    absolute_path = log_path + "//" + month_date + "//" + application_name + "//" + application_id + "//" + err_log + "//";
                }
                bool exists = Directory.Exists(absolute_path);
                if (!exists)
                    Directory.CreateDirectory(absolute_path);
                FileStream fp = new FileStream(absolute_path + DateTime.Now.Year.ToString() + DateTime.Now.Date.Month.ToString("d2") + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + "_log.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fp);
                sw.WriteLine(DateTime.Now.ToString() + " ==> " + log_data);
                sw.Close();
                fp.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error on save log" + ex.ToString());
            }
        }
        #endregion

    }
}
