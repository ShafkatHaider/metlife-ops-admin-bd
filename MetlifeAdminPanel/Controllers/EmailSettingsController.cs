﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class EmailSettingsController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        

        //
        // GET: /EmailSettings/Details/5

        public ActionResult Details()
        {
            if (!DBContext.hasPermission("Metlife Email Settings"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {   
                EmailSetting emailsettings = db.EmailSettings.FirstOrDefault();
                if (emailsettings == null)
                {
                    return HttpNotFound();
                }
                return View(emailsettings);
            }
        }

        
        //
        // GET: /EmailSettings/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife Email Settings"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                EmailSetting emailsettings = db.EmailSettings.Find(id);
                if (emailsettings == null)
                {
                    return HttpNotFound();
                }
                return View(emailsettings);
            }
        }

        //
        // POST: /EmailSettings/Edit/5

        [HttpPost]
        public ActionResult Edit(EmailSetting emailsettings)
        {
            if (!DBContext.hasPermission("Metlife Email Settings"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    db.Entry(emailsettings).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Details");
                }
                return View(emailsettings);
            }
        }

        
    }
}