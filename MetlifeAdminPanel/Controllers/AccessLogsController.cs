﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class AccessLogsController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /AccessLogs/

        public ActionResult Index()
        {
            var access_logs = db.Access_Logs.Include(a => a.Menu).Include(a => a.User_Info);
            return View(access_logs.ToList());
        }

        //
        // GET: /AccessLogs/Details/5

        public ActionResult Details(int id = 0)
        {
            Access_Logs access_logs = db.Access_Logs.Find(id);
            if (access_logs == null)
            {
                return HttpNotFound();
            }
            return View(access_logs);
        }

        //
        // GET: /AccessLogs/Create

        public ActionResult Create()
        {
            ViewBag.ACCESS_MENU = new SelectList(db.Menus, "ID", "URL");
            ViewBag.USER_ID = new SelectList(db.User_Info, "ID", "USERID");
            return View();
        }

        //
        // POST: /AccessLogs/Create

        [HttpPost]
        public ActionResult Create(Access_Logs access_logs)
        {
            if (ModelState.IsValid)
            {
                db.Access_Logs.Add(access_logs);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ACCESS_MENU = new SelectList(db.Menus, "ID", "URL", access_logs.ACCESS_MENU);
            ViewBag.USER_ID = new SelectList(db.User_Info, "ID", "USERID", access_logs.USER_ID);
            return View(access_logs);
        }

        //
        // GET: /AccessLogs/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Access_Logs access_logs = db.Access_Logs.Find(id);
            if (access_logs == null)
            {
                return HttpNotFound();
            }
            ViewBag.ACCESS_MENU = new SelectList(db.Menus, "ID", "URL", access_logs.ACCESS_MENU);
            ViewBag.USER_ID = new SelectList(db.User_Info, "ID", "USERID", access_logs.USER_ID);
            return View(access_logs);
        }

        //
        // POST: /AccessLogs/Edit/5

        [HttpPost]
        public ActionResult Edit(Access_Logs access_logs)
        {
            if (ModelState.IsValid)
            {
                db.Entry(access_logs).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ACCESS_MENU = new SelectList(db.Menus, "ID", "URL", access_logs.ACCESS_MENU);
            ViewBag.USER_ID = new SelectList(db.User_Info, "ID", "USERID", access_logs.USER_ID);
            return View(access_logs);
        }

        //
        // GET: /AccessLogs/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Access_Logs access_logs = db.Access_Logs.Find(id);
            if (access_logs == null)
            {
                return HttpNotFound();
            }
            return View(access_logs);
        }

        //
        // POST: /AccessLogs/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Access_Logs access_logs = db.Access_Logs.Find(id);
            db.Access_Logs.Remove(access_logs);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}