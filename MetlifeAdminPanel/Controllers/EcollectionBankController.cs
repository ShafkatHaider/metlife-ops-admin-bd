﻿using MetlifeAdminPanel.Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MetlifeAdminPanel.Controllers
{
    public class EcollectionBankController : Controller
    {
        private OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();

        //
        // GET: /EcollectionBank/

        public ActionResult Index()
        {
            if (!DBContext.hasPermission("Metlife E-Collection Bank Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View(db.ECollectionBanks.ToList());
            }
        }

        //
        // GET: /EcollectionBank/Details/5

        public ActionResult Details(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife E-Collection Bank Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ECollectionBank ecollectionbank = db.ECollectionBanks.Find(id);
                if (ecollectionbank == null)
                {
                    return HttpNotFound();
                }
                return View(ecollectionbank);
            }
        }

        //
        // GET: /EcollectionBank/Create

        public ActionResult Create()
        {
            if (!DBContext.hasPermission("Metlife E-Collection Bank Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                return View();
            }
        }

        //
        // POST: /EcollectionBank/Create

        [HttpPost]
        public ActionResult Create(ECollectionBank ecollectionbank)
        {
            if (!DBContext.hasPermission("Metlife E-Collection Bank Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    db.ECollectionBanks.Add(ecollectionbank);
                    db.SaveChanges();
                    TempData["success"] = "E-Collection Bank added successfully";
                    return RedirectToAction("Index");
                }
                else {
                    TempData["failed"] = "E-Collection Bank registration failed";
                }

                return View(ecollectionbank);
            }
        }

        //
        // GET: /EcollectionBank/Edit/5

        public ActionResult Edit(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife E-Collection Bank Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ECollectionBank ecollectionbank = db.ECollectionBanks.Find(id);
                if (ecollectionbank == null)
                {
                    return HttpNotFound();
                }
                return View(ecollectionbank);
            }
        }

        //
        // POST: /EcollectionBank/Edit/5

        [HttpPost]
        public ActionResult Edit(ECollectionBank ecollectionbank)
        {
            if (!DBContext.hasPermission("Metlife E-Collection Bank Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    db.Entry(ecollectionbank).State = EntityState.Modified;
                    db.SaveChanges();
                    TempData["success"] = "E-Collection Bank updated successfully";
                    return RedirectToAction("Index");
                }
                else {
                    TempData["failed"] = "E-Collection Bank update failed";
                }
                return View(ecollectionbank);
            }
        }

        //
        // GET: /EcollectionBank/Delete/5

        public ActionResult Delete(int id = 0)
        {
            if (!DBContext.hasPermission("Metlife E-Collection Bank Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ECollectionBank ecollectionbank = db.ECollectionBanks.Find(id);
                if (ecollectionbank == null)
                {
                    return HttpNotFound();
                }
                return View(ecollectionbank);
            }
        }

        //
        // POST: /EcollectionBank/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            if (!DBContext.hasPermission("Metlife E-Collection Bank Management"))
            {
                return RedirectToAction("Action", "Error");
            }
            else
            {
                ECollectionBank ecollectionbank = db.ECollectionBanks.Find(id);
                db.ECollectionBanks.Remove(ecollectionbank);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}