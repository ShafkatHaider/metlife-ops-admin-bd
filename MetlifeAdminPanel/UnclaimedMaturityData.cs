//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MetlifeAdminPanel
{
    using System;
    using System.Collections.Generic;
    
    public partial class UnclaimedMaturityData
    {
        public int ID { get; set; }
        public string BatchID { get; set; }
        public byte[] PolicyNo { get; set; }
        public byte[] POName { get; set; }
        public byte[] YearOfBirth { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string BenefitType { get; set; }
        public Nullable<System.DateTime> CreationTime { get; set; }
        public Nullable<System.DateTime> UpdationTime { get; set; }
    }
}
