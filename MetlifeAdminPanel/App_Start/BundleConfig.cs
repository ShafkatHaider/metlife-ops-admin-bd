﻿using System.Web;
using System.Web.Optimization;

namespace MetlifeAdminPanel
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));
            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include("~/Scripts/jquery-ui-{version}.js"));
            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include("~/Scripts/jquery.unobtrusive*","~/Scripts/jquery.validate*"));
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-*"));
            //bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            //bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
            //    "~/Content/themes/base/jquery.ui.core.css",
            //    "~/Content/themes/base/jquery.ui.resizable.css",
            //    "~/Content/themes/base/jquery.ui.selectable.css",
            //    "~/Content/themes/base/jquery.ui.accordion.css",
            //    "~/Content/themes/base/jquery.ui.autocomplete.css",
            //    "~/Content/themes/base/jquery.ui.button.css",
            //    "~/Content/themes/base/jquery.ui.dialog.css",
            //    "~/Content/themes/base/jquery.ui.slider.css",
            //    "~/Content/themes/base/jquery.ui.tabs.css",
            //    "~/Content/themes/base/jquery.ui.datepicker.css",
            //    "~/Content/themes/base/jquery.ui.progressbar.css",
            //    "~/Content/themes/base/jquery.ui.theme.css"));


            BundleTable.EnableOptimizations = false;

            bundles.Add(new ScriptBundle("~/bundles/my_style/jquery").Include(
            "~/Content/my_style/plugins/bower_components/jquery/dist/jquery.js",
            "~/Content/my_style/bootstrap/dist/js/tether.js",
            "~/Content/my_style/bootstrap/dist/js/bootstrap.js",
            "~/Content/my_style/js/jasny-bootstrap.js",
            "~/Content/my_style/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.js",
            "~/Content/my_style/plugins/bower_components/sidebar-nav/dist/sidebar-nav.js",
            "~/Content/my_style/js/jquery.slimscroll.js",
            "~/Content/my_style/js/waves.js",
            "~/Content/my_style/js/custom.js",
            "~/Content/my_style/js/jquery-check-all.js",
            "~/Content/my_style/plugins/bower_components/moment/moment.js",
	        "~/Content/my_style/plugins/bower_components/datatables/jquery.dataTables.js",
            "~/Content/my_style/downloaded/js/dataTables.buttons.js",
            "~/Content/my_style/downloaded/js/buttons.flash.js",
            "~/Content/my_style/downloaded/js/jszip.js",
            "~/Content/my_style/downloaded/js/pdfmake.js",
            "~/Content/my_style/downloaded/js/vfs_fonts.js",
            "~/Content/my_style/downloaded/js/buttons.html5.js",
            "~/Content/my_style/downloaded/js/buttons.print.js",	
            "~/Content/my_style/plugins/bower_components/sweetalert/sweetalert.js",
            "~/Content/my_style/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js",
            "~/Content/my_style/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.js",
            "~/Content/my_style/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"));



            //bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/my_style/css/style.css"));
            bundles.Add(new StyleBundle("~/Content/my_style").Include(
            "~/Content/my_style/plugins/images/favicon.png",
            "~/Content/my_style/bootstrap/dist/css/bootstrap.css",
            "~/Content/my_style/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css",
            "~/Content/my_stylen/css/animate.css",
            "~/Content/my_style/plugins/bower_components/sidebar-nav/dist/sidebar-nav.css",
            "~/Content/my_style/plugins/bower_components/datatables/jquery.dataTables.css",
            "~/Content/my_style/downloaded/css/buttons.dataTables.css",
            "~/Content/my_style/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.css",
            "~/Content/my_style/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css",
            "~/Content/my_style/css/animate.css",
            "~/Content/my_style/css/style.css",
            "~/Content/my_style/css/colors/gray-dark.css",
            "~/Content/my_style/plugins/bower_components/sweetalert/sweetalert.css",
            "~/Content/my_style/downloaded/js/html5shiv.js",
            "~/Content/my_style/downloaded/js/respond.min.js"));


            bundles.Add(new StyleBundle("~/Content/my_style/image").Include(
            "~/Content/my_style/plugins/images/failed.jpg",
            "~/Content/my_style/plugins/images/ok.jpg"));
           

        }
    }
}