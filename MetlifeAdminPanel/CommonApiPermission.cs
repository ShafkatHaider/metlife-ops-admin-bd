//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MetlifeAdminPanel
{
    using System;
    using System.Collections.Generic;
    
    public partial class CommonApiPermission
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CommonApiPermission()
        {
            this.CommonApiRolePermissions = new HashSet<CommonApiRolePermission>();
        }
    
        public int ID { get; set; }
        public string NAME { get; set; }
        public Nullable<int> MENU_ID { get; set; }
        public Nullable<int> IS_ACTIVE { get; set; }
    
        public virtual CommonApiMenu CommonApiMenu { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CommonApiRolePermission> CommonApiRolePermissions { get; set; }
    }
}
