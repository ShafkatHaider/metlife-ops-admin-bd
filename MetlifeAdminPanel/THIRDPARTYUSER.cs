//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MetlifeAdminPanel
{
    using System;
    using System.Collections.Generic;
    
    public partial class THIRDPARTYUSER
    {
        public long ID { get; set; }
        public byte[] USERNAME { get; set; }
        public byte[] CLIENTNAME { get; set; }
        public byte[] PASSWORD { get; set; }
        public byte[] EMAIL { get; set; }
        public byte[] TRANSPREFIX { get; set; }
        public byte[] TOKENEXPIRY { get; set; }
        public bool STATUS { get; set; }
        public byte[] GATEWAYID { get; set; }
        public byte[] APIKEY { get; set; }
        public byte[] CLIENTTYPE { get; set; }
        public Nullable<System.DateTime> CREATEDATE { get; set; }
        public Nullable<long> CREATEDBY { get; set; }
        public Nullable<System.DateTime> UPDATEDATE { get; set; }
        public Nullable<long> UPDATEBY { get; set; }
    }
}
