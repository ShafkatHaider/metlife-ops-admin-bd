﻿using MetlifeAdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MetlifeAdminPanel.Services
{
    public class Data
    {
        public Menu menu { get; set; }
        public string parentMenu { get; set; }
        public Data()
        {
            this.menu = null;
            this.parentMenu = "";
        }

    }
    public class ExcelDataD2C
    {
        public string SerialOfDay { get; set; }
        public string TransactionNo { get; set; }
        public string PaymentDate { get; set; }
        public string PaidFor { get; set; }
        public string ApplicationType { get; set; }
        public string PolicyNumber { get; set; }
        public string Amount { get; set; }
        public string Status { get; set; }
        public string POName { get; set; }
        public string AgentCode { get; set; }
        public string PayerEmail { get; set; }
        public string BOCard { get; set; }
        public string GatewayTransactionID { get; set; }
        public string ClientName { get; set; }
 
    }
    public class ExcelData {
        
        public string SerialOfDay { get; set; }
        public string GatewayCardNumber { get; set; }
        public string refID { get; set; }

        public string GatewayRespondTime { get; set; }
        public string PaidFor { get; set; }
        public string PaidForDetail { get; set; }

        public string PolicyNumber { get; set; }
        public string ReceiptHeading { get; set; }
        public decimal GrandAmount { get; set; }
        public string GatewayStatus { get; set; }
        public string POName { get; set; }
        public string Agent { get; set; }
        public string PayerEmail { get; set; }       
        public string GatewayCardBrand { get; set; }
        public string GatewayTransactionID { get; set; }
    }


    public class ExcelDataSubmission
    {
        public string PolicyNumber { get; set; }
        public string YearOfBirth { get; set; }
        public string POName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string BenefitType { get; set; }
        public string CreationTime { get; set; }   

    }


    public class ExcelDatForEcollection
    {
        public string Orderid { get; set; }
        public string beneficiaryName { get; set; }
        public string bankAccountNumber { get; set; }
        public string payingBank { get; set; }
        public decimal Amount { get; set; }
        public string bankName { get; set; }
        public string branch { get; set; }       
    }

    public class DataObjectsForRolePermission { 
        public Role_Permission rolePermission { get; set; }
        public Role role { get; set; }
        public DataObjectsForRolePermission()
        {
            this.rolePermission = null;
            this.role = null;
        }
    }

    public class ReportManager {
        public DummySearch dummySearch { get; set; }
        public List<Transaction> tranList { get; set; }
        
        public ReportManager()
        {
            this.dummySearch = new DummySearch();
            this.dummySearch.customerName = "";
            this.dummySearch.payerEmail = "";
            this.dummySearch.poEmail = "";
            this.dummySearch.downloadStatus = 3;
            this.dummySearch.policyNumber = "";
            this.dummySearch.type = "";
            this.dummySearch.status = "";
            this.dummySearch.startDate = "";
            this.dummySearch.endDate = "";
            this.dummySearch.gateways = "";
            this.tranList = null;
        }

        public class SubmissionReportManager
        {
            public DummySubmissionSearch dummySearch { get; set; }
            public List<UnclaimedSubmission> tranList { get; set; }

            public SubmissionReportManager()
            {
                this.dummySearch = new DummySubmissionSearch();
                this.dummySearch.PolicyNumber = "";
                this.dummySearch.POName = "";
                this.dummySearch.YearOfBirth = "";
                this.dummySearch.Email = "";
                this.dummySearch.downloadStatus = 100;
                this.dummySearch.Phone = "";
                this.dummySearch.BenefitType = "";

                this.tranList = null;
            }
        }
      

        public static DataSet ListToDataSet<T>(List<T> listData)
        {
            Type elementType = typeof(T);
            DataSet ds = new DataSet();
            DataTable t = new DataTable();
            ds.Tables.Add(t);

            //add a column to table for each public property on T
            foreach (var propInfo in elementType.GetProperties())
            {
                Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;
                t.Columns.Add(propInfo.Name, ColType);
            }

            //go through each property on T and add each value to the table
            foreach (var item in listData)
            {
                DataRow row = t.NewRow();
                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }
                t.Rows.Add(row);
            }
            return ds;
        }
    }

    public class ReportManagerD2C
    {
        public DummySearch dummySearch { get; set; }
        public List<D2C_Transaction_Log> tranList { get; set; }

        public ReportManagerD2C()
        {
            this.dummySearch = new DummySearch();
            this.dummySearch.customerName = "";
            this.dummySearch.payerEmail = "";
            this.dummySearch.poEmail = "";
            this.dummySearch.downloadStatus = 3;
            this.dummySearch.policyNumber = "";
            this.dummySearch.type = "";
            this.dummySearch.status = "";
            this.dummySearch.startDate = "";
            this.dummySearch.endDate = "";
            this.dummySearch.gateways = "";
            this.tranList = null;
        }

        public class SubmissionReportManager
        {
            public DummySubmissionSearch dummySearch { get; set; }
            public List<UnclaimedSubmission> tranList { get; set; }

            public SubmissionReportManager()
            {
                this.dummySearch = new DummySubmissionSearch();
                this.dummySearch.PolicyNumber = "";
                this.dummySearch.POName = "";
                this.dummySearch.YearOfBirth = "";
                this.dummySearch.Email = "";
                this.dummySearch.downloadStatus = 100;
                this.dummySearch.Phone = "";
                this.dummySearch.BenefitType = "";

                this.tranList = null;
            }
        }


        public static DataSet ListToDataSet<T>(List<T> listData)
        {
            Type elementType = typeof(T);
            DataSet ds = new DataSet();
            DataTable t = new DataTable();
            ds.Tables.Add(t);

            //add a column to table for each public property on T
            foreach (var propInfo in elementType.GetProperties())
            {
                Type ColType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;
                t.Columns.Add(propInfo.Name, ColType);
            }

            //go through each property on T and add each value to the table
            foreach (var item in listData)
            {
                DataRow row = t.NewRow();
                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }
                t.Rows.Add(row);
            }
            return ds;
        }
    }
    public class PaymentVerificationManager
    {
        public PaymentVerification dummySearch { get; set; }
        public List<Transaction> tranList { get; set; }

        public PaymentVerificationManager()
        {
            this.dummySearch = new PaymentVerification();
            this.dummySearch.policyNumber = "";
            this.dummySearch.yearOfBirth = "";
            this.tranList = null;
        }
    }


    public class EcollectionManager
    {
        public ECollectionReport dummySearch { get; set; }
        public List<ECollection> tranList { get; set; }

        public EcollectionManager()
        {
            this.dummySearch = new ECollectionReport();
            this.dummySearch.orderId = "";
            this.dummySearch.beneficiaryName = "";
            this.dummySearch.bankAccountNumber = "";
            this.dummySearch.fromAmount = "";
            this.dummySearch.toAmount = "";
            this.dummySearch.payingBank = "";
            this.tranList = null;
        }
    }

    public class DashboardManager
    {
        public List<Dashboard> countForToday { get; set; }
        public List<Dashboard> countTotal { get; set; }        
        public List<Dashboard> unclaimedmaturityupload { get; set; }

    }

}