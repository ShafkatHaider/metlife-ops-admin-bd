﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MetlifeAdminPanel.Services
{
    public class DataLog
    {
        #region Log Create

        public static string log_path = ConfigurationManager.AppSettings["ExceptionLogPath"];
        public static string APPLICATION_ID = ConfigurationManager.AppSettings["ApplicationId"];
        public string savelog(String log, bool is_server)
        {
            CreateLogs(log_path, "METLIFE_BD_Admin_Exception", APPLICATION_ID, is_server, log);
            return "";
        }
        public static void CreateLogs(string log_path, string application_name, string application_id, bool is_server, string log_data)
        {

            String ser_log = "SerLogs";
            String err_log = "ErrorLogs";
            String absolute_path = "";
            try
            {
                String month_date = DateTime.Now.Year.ToString() + DateTime.Now.Date.Month.ToString("d2");
                if (is_server)
                {
                    absolute_path = log_path + month_date + "//" + application_name + "//" + application_id + "//" + ser_log + "//";
                }
                else
                {
                    absolute_path = log_path + "//" + month_date + "//" + application_name + "//" + application_id + "//" + err_log + "//";
                }
                bool exists = System.IO.Directory.Exists(absolute_path);
                if (!exists)
                    Directory.CreateDirectory(absolute_path);
                FileStream fp = new FileStream(absolute_path + DateTime.Now.Year.ToString() + DateTime.Now.Date.Month.ToString("d2") + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + "_log.txt", FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fp);
                sw.WriteLine(DateTime.Now.ToString() + " ==> " + log_data);
                sw.Close();
                fp.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error on save log" + ex.ToString());
            }
        }
        #endregion
    }
}
