﻿using MetlifeAdminPanel.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MetlifeAdminPanel.Services
{
    public class DBContext
    {

        public static List<DisplayTable> GetMenuList()
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<DisplayTable>();
            List<Menu> listMenu = db.Menus.Where(b => b.IS_ACTIVE == 1).ToList();
            foreach (Menu menu in listMenu)
            {
                list.Add(new DisplayTable
                {
                    Key = menu.ID.ToString(),
                    Display = menu.URL.ToString()
                });
            }
            return list;
        }

        public static List<DisplayTable> GetUserList()
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<DisplayTable>();
            List<User_Info> listUsers = db.User_Info.Where(b => b.STATUS == "Active").ToList();
            foreach (User_Info user_info in listUsers)
            {
                list.Add(new DisplayTable
                {
                    Key = user_info.ID.ToString(),
                    Display = user_info.NAME.ToString()
                });
            }
            return list;
        }


        public static List<DisplayTable> GetRoleList()
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<DisplayTable>();
            List<Role> listRoles = db.Roles.Where(b => b.IS_ACTIVE == 1).ToList();
            foreach (Role role in listRoles)
            {
                list.Add(new DisplayTable
                {
                    Key = role.ID.ToString(),
                    Display = role.NAME.ToString()
                });
            }
            return list;
        }


        public static List<DisplayTable> GetPermissionList()
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<DisplayTable>();
            List<Permission> listPermissions = db.Permissions.Where(b => b.IS_ACTIVE == 1).ToList();
            foreach (Permission permission in listPermissions)
            {
                list.Add(new DisplayTable
                {
                    Key = permission.ID.ToString(),
                    Display = permission.NAME.ToString()
                });
            }
            return list;
        }

        public static List<UnclaimedMaturityLog> GetPendingMaturityFileList()
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            List<UnclaimedMaturityLog> listUnclaimedMaturityLog = db.UnclaimedMaturityLogs.Where(b => b.Status == "processing").ToList();
            return listUnclaimedMaturityLog;
        }

        public static List<DisplayTable> GetPaymentTypeList()
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<DisplayTable>();
            List<PaymentType> listPaymentType = db.PaymentTypes.ToList();
            foreach (PaymentType paymentType in listPaymentType)
            {
                list.Add(new DisplayTable
                {
                    Key = paymentType.Slug.ToString(),
                    Display = paymentType.Name.ToString()
                });
            }
            return list;
        }
        public static List<DisplayTable> GetPaymentGateways()
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<DisplayTable>();
            List<PaymentGateway> listPaymentgateways = db.PaymentGateways.ToList();
            foreach (PaymentGateway paymentGateway in listPaymentgateways)
            {
                list.Add(new DisplayTable
                {
                    Key = paymentGateway.GateWayName.ToString(),
                    Display = paymentGateway.GateWayName.ToString()
                });
            }
            return list;
        }
        public static List<DisplayTable> GetPayingBankList()
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<DisplayTable>();
            List<ECollectionBank> listPayingBank = db.ECollectionBanks.ToList();
            foreach (ECollectionBank payingBank in listPayingBank)
            {
                list.Add(new DisplayTable
                {
                    Key = payingBank.ID.ToString(),
                    Display = payingBank.BankName.ToString()
                });
            }
            return list;
        }

        public static IEnumerable<SelectListItem> GetpermissionSelectList(int roleId)
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            List<Permission> listPermissions = db.Permissions.Where(b => b.IS_ACTIVE == 1).ToList();
            List<SelectListItem> permissionSelectList = new List<SelectListItem>();
            List<Role_Permission> listRolePermissions = db.Role_Permission.Where(b => b.ROLE_ID == roleId).ToList();
            foreach (Permission permission in listPermissions)
            {
                SelectListItem Item = new SelectListItem();
                Item.Text = permission.NAME;
                Item.Value = permission.ID.ToString();
                foreach (Role_Permission rolePerm in listRolePermissions)
                {
                    if (rolePerm.PERMISSION_ID == permission.ID)
                    {
                        Item.Selected = true;
                    }
                }
                permissionSelectList.Add(Item);
            }
            return permissionSelectList;
        }

        public static string getRoleofUser(String userName)
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            string roleName = "";
            List<User_Info> listUsers = db.User_Info.Where(b => b.NAME == userName).ToList();
            if (listUsers != null)
            {
                if (listUsers.Count > 0)
                {
                    int userId = listUsers[0].ID;
                    List<User_Role> listUserRole = db.User_Role.Where(b => b.USER_ID == userId).ToList();
                    if (listUserRole != null)
                    {
                        if (listUserRole.Count > 0)
                        {
                            int roleId = (int)listUserRole[0].ROLE_ID;
                            List<Role> listRole = db.Roles.Where(b => b.ID == roleId).ToList();
                            roleName = listRole[0].NAME.ToString();
                        }
                    }
                }
            }
            return roleName;
        }


        public static bool IsFirstAdminExist(string userName, string password)
        {
            password = Util.Encrypt(password, true);
            //password = Util.Decrypt(password, true);

            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            List<User_Info> listUsers = db.User_Info.Where(b => b.NAME == userName && b.PASSWORD == password).ToList();
            if (listUsers != null)
            {
                if (listUsers.Count > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public static string GetEcollectionBankById(string id)
        {
            int intId = Int32.Parse(id);
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            ECollectionBank eCollectionBank = db.ECollectionBanks.Where(b => b.ID == intId).SingleOrDefault();
            if (eCollectionBank != null)
            {
                string bankName = eCollectionBank.BankName;
                return bankName;
            }
            else
            {
                return "";
            }
        }


        public static bool hasPermission(String permissionName)
        {
            try
            {
                string sessionRoleName = System.Web.HttpContext.Current.Session["roleName"].ToString();
                OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
                List<Role> listRoles = db.Roles.Where(b => b.IS_ACTIVE == 1 && b.NAME == sessionRoleName).ToList();
                int roleId = listRoles[0].ID;
                List<Permission> listPermissions = db.Permissions.Where(b => b.IS_ACTIVE == 1 && b.NAME == permissionName).ToList();
                int permissionId = listPermissions[0].ID;

                List<Role_Permission> rolePermissions = db.Role_Permission.Where(b => b.ROLE_ID == roleId && b.PERMISSION_ID == permissionId).ToList();

                if (rolePermissions != null)
                {
                    if (rolePermissions.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static List<D2C_Transaction_Log> GetD2CLog(ReportManagerD2C reportManager)
        {
            DateTime start = DateTime.Now.Date;
            DateTime end = DateTime.Now.Date;
            if (reportManager.dummySearch.startDate != null && reportManager.dummySearch.endDate != null
                && reportManager.dummySearch.startDate != "" && reportManager.dummySearch.endDate != "")
            {
                start = DateManager.GetDate(reportManager.dummySearch.startDate, true, false) ?? DateTime.Now;
                end = DateManager.GetDate(reportManager.dummySearch.endDate, true, true) ?? DateTime.Now;
            }
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<D2C_Transaction_Log>();

            var listTransaction = db.D2C_Transaction_Log
           .Where(
                data =>
                   (reportManager.dummySearch.policyNumber != null ? data.PolicyNumber == reportManager.dummySearch.policyNumber : "1" == "1")
                && (reportManager.dummySearch.gateways != null && reportManager.dummySearch.gateways != "" ? data.ClientName == reportManager.dummySearch.gateways : "1" == "1")
                && ((reportManager.dummySearch.startDate != null && reportManager.dummySearch.endDate != null && reportManager.dummySearch.startDate != "" && reportManager.dummySearch.endDate != null) ? data.CreateDate >= start && data.CreateDate <= end : "1" == "1")
             ).OrderByDescending(data => data.Id)
             .ToList();

            foreach (var trans in listTransaction)
            {
                list.Add(trans);
            }



            return list;
        }

        public static List<Transaction> GetTransactionList(ReportManager reportManager)
        {
            DateTime start = DateTime.Now.Date;
            DateTime end = DateTime.Now.Date;
            if (reportManager.dummySearch.startDate != null && reportManager.dummySearch.endDate != null
                && reportManager.dummySearch.startDate != "" && reportManager.dummySearch.endDate != "")
            {
                start = DateManager.GetDate(reportManager.dummySearch.startDate, true, false) ?? DateTime.Now;
                end = DateManager.GetDate(reportManager.dummySearch.endDate, true, true) ?? DateTime.Now;
            }
            Enc enc = new Enc();
            byte[] policyNumber = null;
            if (reportManager.dummySearch.policyNumber != null && reportManager.dummySearch.policyNumber != "")
            {
                policyNumber = enc.Encrypt(reportManager.dummySearch.policyNumber);
            }

            /*
            byte[] transactionId = null;
            if (reportManager.dummySearch.TransactionID != null && reportManager.dummySearch.TransactionID != "")
            {
                transactionId = enc.Encrypt(reportManager.dummySearch.TransactionID);
            }
            byte[] payerEmail = null;
            if (reportManager.dummySearch.payerEmail != null && reportManager.dummySearch.payerEmail != "")
            {
                payerEmail = enc.Encrypt(reportManager.dummySearch.payerEmail);
            }
            byte[] poEmail = null;
            if (reportManager.dummySearch.poEmail != null && reportManager.dummySearch.poEmail != "")
            {
                poEmail = enc.Encrypt(reportManager.dummySearch.poEmail);
            }*/
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<Transaction>();

            // && (transactionId != null ? data.TransactionID == transactionId : "1" == "1")
            //&& (payerEmail != null ? data.PayerEmail == payerEmail : "1" == "1")
            //&& (poEmail != null ? data.POEmail == poEmail : "1" == "1")
            //&& (reportManager.dummySearch.downloadStatus != null && reportManager.dummySearch.downloadStatus != 100 ? data.ExcelDownloaded == reportManager.dummySearch.downloadStatus : "1" == "1")
            //&& (reportManager.dummySearch.status != null && reportManager.dummySearch.status != "" ? data.Status == reportManager.dummySearch.status : "1" == "1")

            if (reportManager.dummySearch.gateways == "Card")
            {
                var listTransaction = db.Transactions
        .Where(
             data =>
                (policyNumber != null ? data.PolicyNumber == policyNumber : "1" == "1")
             && (reportManager.dummySearch.type != null && reportManager.dummySearch.type != "" ? data.PurposeOfPayment == reportManager.dummySearch.type : "1" == "1")
             && (reportManager.dummySearch.gateways != null && reportManager.dummySearch.gateways != "" ? data.GatewayName != "Bkash" : "1" == "1")
             && ((reportManager.dummySearch.startDate != null && reportManager.dummySearch.endDate != null && reportManager.dummySearch.startDate != "" && reportManager.dummySearch.endDate != null) ? data.PaymentDate >= start && data.PaymentDate <= end : "1" == "1")
             && (data.ReceiptHeading != null)
          ).OrderByDescending(data => data.ID)
          .ToList();

                foreach (var trans in listTransaction)
                {
                    list.Add(trans);
                }
            }

            else
            {
                var listTransaction = db.Transactions
        .Where(
             data =>
                (policyNumber != null ? data.PolicyNumber == policyNumber : "1" == "1")
             && (reportManager.dummySearch.type != null && reportManager.dummySearch.type != "" ? data.PurposeOfPayment == reportManager.dummySearch.type : "1" == "1")
             && (reportManager.dummySearch.gateways != null && reportManager.dummySearch.gateways != "" ? data.GatewayName == reportManager.dummySearch.gateways : "1" == "1")
             && ((reportManager.dummySearch.startDate != null && reportManager.dummySearch.endDate != null && reportManager.dummySearch.startDate != "" && reportManager.dummySearch.endDate != null) ? data.PaymentDate >= start && data.PaymentDate <= end : "1" == "1")
             && (data.ReceiptHeading != null)
          ).OrderByDescending(data => data.ID)
          .ToList();

                foreach (var trans in listTransaction)
                {
                    list.Add(trans);
                }
            }


            return list;
        }


        public static List<UnclaimedSubmission> GetUnclaimedSubmissionList(ReportManager.SubmissionReportManager reportManager)
        {

            DateTime start = DateTime.Now.Date;
            DateTime end = DateTime.Now.Date;
            if (reportManager.dummySearch.startDate != null && reportManager.dummySearch.endDate != null
                && reportManager.dummySearch.startDate != "" && reportManager.dummySearch.endDate != "")
            {
                start = DateManager.GetDate(reportManager.dummySearch.startDate, true, false) ?? DateTime.Now;
                end = DateManager.GetDate(reportManager.dummySearch.endDate, true, true) ?? DateTime.Now;
            }

            Enc enc = new Enc();
            byte[] policyNumber = null;
            if (reportManager.dummySearch.PolicyNumber != null && reportManager.dummySearch.PolicyNumber != "")
            {
                policyNumber = enc.Encrypt(reportManager.dummySearch.PolicyNumber);
            }
            byte[] POName = null;
            if (reportManager.dummySearch.POName != null && reportManager.dummySearch.POName != "")
            {
                POName = enc.Encrypt(reportManager.dummySearch.POName);
            }
            byte[] YearOfBirth = null;
            if (reportManager.dummySearch.YearOfBirth != null && reportManager.dummySearch.YearOfBirth != "")
            {
                YearOfBirth = enc.Encrypt(reportManager.dummySearch.YearOfBirth);
            }
            byte[] phone = null;
            if (reportManager.dummySearch.Phone != null && reportManager.dummySearch.Phone != "")
            {
                phone = enc.Encrypt(reportManager.dummySearch.Phone);
            }

            byte[] email = null;
            if (reportManager.dummySearch.Email != null && reportManager.dummySearch.Email != "")
            {
                email = enc.Encrypt(reportManager.dummySearch.Email);
            }

            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<UnclaimedSubmission>();
            var listTransaction = db.UnclaimedSubmissions
                    .Where(
                         data =>
                            (policyNumber != null ? data.PolicyNumber == policyNumber : "1" == "1")
                            && (POName != null ? data.POName == POName : "1" == "1")
                            && (YearOfBirth != null ? data.YearOfBirth == YearOfBirth : "1" == "1")
                            && (email != null ? data.Email == email : "1" == "1")
                            && (phone != null ? data.Phone == phone : "1" == "1")
                            && (reportManager.dummySearch.downloadStatus != null && reportManager.dummySearch.downloadStatus != 3 ? data.Downloaded == reportManager.dummySearch.downloadStatus : "1" == "1")
                            && ((reportManager.dummySearch.startDate != null && reportManager.dummySearch.endDate != null && reportManager.dummySearch.startDate != "" && reportManager.dummySearch.endDate != null) ? data.CreationTime >= start && data.CreationTime <= end : "1" == "1")
                            ).OrderByDescending(data => data.ID)
                      .ToList();

            foreach (var trans in listTransaction)
            {
                list.Add(trans);
            }
            return list;
        }



        public static List<Transaction> GetTransactionList(PaymentVerificationManager reportManager)
        {
            DateTime start = DateTime.Now.Date;
            DateTime end = DateTime.Now.Date;
            Enc enc = new Enc();

            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<Transaction>();

            byte[] policyNumber = null;
            if (reportManager.dummySearch.policyNumber != null && reportManager.dummySearch.policyNumber != "")
            {
                policyNumber = enc.Encrypt(reportManager.dummySearch.policyNumber);
            }
            byte[] YearOfBirth = null;
            if (reportManager.dummySearch.yearOfBirth != null && reportManager.dummySearch.yearOfBirth != "")
            {
                YearOfBirth = enc.Encrypt(reportManager.dummySearch.yearOfBirth);
            }
            var listTransaction = db.Transactions.Where(
                                  data =>
                                  (policyNumber != null ? data.PolicyNumber == policyNumber : "1" == "1")
                                  && (YearOfBirth != null ? data.YearOfBirth == YearOfBirth : "1" == "1")
                                  ).ToList();

            foreach (var trans in listTransaction)
            {
                list.Add(trans);
            }
            return list;
        }


        public static List<ECollection> GetEcollectionList(EcollectionManager reportManager)
        {

            decimal fromAmount = 0;
            decimal toAmount = 0;

            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            var list = new List<ECollection>();
            if (reportManager.dummySearch.fromAmount != null && reportManager.dummySearch.fromAmount != "")
            {
                fromAmount = Convert.ToDecimal(reportManager.dummySearch.fromAmount);
            }

            if (reportManager.dummySearch.toAmount != null && reportManager.dummySearch.toAmount != "")
            {
                toAmount = Convert.ToDecimal(reportManager.dummySearch.toAmount);
            }

            var listTransaction = db.ECollections
                                  .Where(
                                     data =>
                                     (reportManager.dummySearch.orderId != null && reportManager.dummySearch.orderId != "" ? data.OrderID == reportManager.dummySearch.orderId : "1" == "1")
                                     && (reportManager.dummySearch.beneficiaryName != null && reportManager.dummySearch.beneficiaryName != "" ? data.BeneficiaryName == reportManager.dummySearch.beneficiaryName : "1" == "1")
                                     && (reportManager.dummySearch.bankAccountNumber != null && reportManager.dummySearch.bankAccountNumber != "" ? data.BankAccountNumber == reportManager.dummySearch.bankAccountNumber : "1" == "1")
                                     && (reportManager.dummySearch.payingBank != null && reportManager.dummySearch.payingBank != "" ? data.PayingBank == reportManager.dummySearch.payingBank : "1" == "1")
                                     && ((fromAmount != 0 && toAmount != 0) ? data.Amount >= fromAmount && data.Amount <= toAmount : "1" == "1")
                                     && ((fromAmount != 0 && toAmount == 0) ? data.Amount >= fromAmount : "1" == "1")
                                     && ((fromAmount == 0 && toAmount != 0) ? data.Amount <= toAmount : "1" == "1")
                                   ).OrderByDescending(data => data.ID)
                                   .ToList();

            foreach (var trans in listTransaction)
            {
                list.Add(trans);
            }
            return list;
        }

        public static List<Dashboard> DashboardCountForToday()
        {
            DateTime start = DateTime.Now.Date;

            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            int initCount = db.Transactions.Where(o => o.Status == "init" && o.PaymentDate == start).Count();
            int successCount = db.Transactions.Where(o => o.Status == "success" && o.PaymentDate == start).Count();
            int failedCount = db.Transactions.Where(o => o.Status == "failed" && o.PaymentDate == start).Count();
            int canceledCount = db.Transactions.Where(o => o.Status == "cancel" && o.PaymentDate == start).Count();
            List<Dashboard> dashboardCountList = new List<Dashboard>();
            Dashboard dashBoard = new Dashboard();
            dashBoard.initCount = initCount;
            dashBoard.successCount = successCount;
            dashBoard.failedCount = failedCount;
            dashBoard.canceledCount = canceledCount;
            dashboardCountList.Add(dashBoard);
            return dashboardCountList;
        }
        public static List<Dashboard> DashboardCountForUnclaimed()
        {
            try
            {
                OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
                int failtran = 0;
                string uploadtran = db.UnclaimedMaturityLogs.Where(x => x.Status == "SUCCESS").OrderByDescending(x => x.ID).Select(x => x.DataCount).FirstOrDefault().ToString();
                string successtran = db.UnclaimedMaturityLogs.Where(x => x.Status == "SUCCESS").OrderByDescending(x => x.ID).Select(x => x.TotalUploaded).FirstOrDefault().ToString();
                string totalamount = db.UnclaimedMaturityLogs.Where(x => x.Status == "SUCCESS").OrderByDescending(x => x.ID).Select(x => x.TotalAmount).FirstOrDefault().ToString();
                try
                {
                    failtran = Convert.ToInt32(uploadtran) - Convert.ToInt32(successtran);
                }
                catch (Exception ex)
                {
                    failtran = 0;
                }
                string uploadtime = db.UnclaimedMaturityLogs.Where(x => x.Status == "SUCCESS").OrderByDescending(x => x.ID).Select(x => x.UploadTime).FirstOrDefault().ToString();
                string uploadby = db.UnclaimedMaturityLogs.Where(x => x.Status == "SUCCESS").OrderByDescending(x => x.ID).Select(x => x.UploadedBy).FirstOrDefault().ToString();
                List<Dashboard> dashboardunclaimedCountList = new List<Dashboard>();
                Dashboard dashBoard = new Dashboard();

                dashBoard.totaluploaded = Convert.ToInt32(uploadtran);
                dashBoard.successCount = Convert.ToInt32(successtran);
                dashBoard.totalamountuploaded = Convert.ToDecimal(totalamount);
                dashBoard.failedCount = failtran;
                dashBoard.uploadeddatetime = uploadtime;
                dashBoard.uploadedby = uploadby;
                dashboardunclaimedCountList.Add(dashBoard);
                return dashboardunclaimedCountList;
            }
            catch (Exception e)
            {
                List<Dashboard> dashboardunclaimedCountList = new List<Dashboard>();
                Dashboard dashBoard = new Dashboard();

                dashBoard.totaluploaded = 0;
                dashBoard.successCount = 0;
                dashBoard.totalamountuploaded = 0;
                dashBoard.failedCount = 0;
                dashBoard.uploadeddatetime = "";
                dashBoard.uploadedby = "";
                dashboardunclaimedCountList.Add(dashBoard);
                return dashboardunclaimedCountList;
            }

        }
        public static List<Dashboard> DashboardCountTotal()
        {
            OLPABDDATAEntities_new db = new OLPABDDATAEntities_new();
            int initCount = db.Transactions.Where(o => o.Status == "init").Count();
            int successCount = db.Transactions.Where(o => o.Status == "success").Count();
            int failedCount = db.Transactions.Where(o => o.Status == "failed").Count();
            int canceledCount = db.Transactions.Where(o => o.Status == "cancel").Count();
            List<Dashboard> dashboardCountList = new List<Dashboard>();
            Dashboard dashBoard = new Dashboard();
            dashBoard.initCount = initCount;
            dashBoard.successCount = successCount;
            dashBoard.failedCount = failedCount;
            dashBoard.canceledCount = canceledCount;
            dashboardCountList.Add(dashBoard);
            return dashboardCountList;
        }

        public class DateManager
        {
            static bool IsMonthAssigned { get; set; }
            public static DateTime? GetDate(string d)
            {

                IsMonthAssigned = false;
                char[] splitsoptions = { '/', '-', ' ' };
                foreach (var i in splitsoptions)
                {
                    var y = 0;
                    var m = 0;
                    var day = 0;
                    if (d.IndexOf(i) > 0)
                    {
                        try
                        {
                            foreach (var e in d.Split(i))
                            {
                                if (e.Length == 4)
                                {
                                    y = Convert.ToInt32(e);
                                    continue;
                                }
                                if (Convert.ToInt32(e) <= 12 && !IsMonthAssigned)
                                {
                                    m = Convert.ToInt32(e);
                                    IsMonthAssigned = true;
                                    continue;
                                }
                                day = Convert.ToInt32(e);
                            }
                            return new DateTime(y, m, day);
                        }
                        catch
                        {
                            //We are silent about this but we  could set a message about wrong date input in ViewBag    and display to user if this  this method returns null
                        }
                    }
                }
                return null;


            }
            // Another overload. this will catch more date formats without manually checking as above

            public static DateTime? GetDate(string d, bool custom, bool isEndDate)
            {
                CultureInfo culture = new CultureInfo("en-US");

                string[] dateFormats =
                {
                    "dd/MM/yyyy", "MM/dd/yyyy", "yyyy/MM/dd", "yyyy/dd/MM", "dd-MM-yyyy", "MM-dd-yyyy", "yyyy-MM-dd",
                    "yyyy-dd-MM", "dd MM yyyy", "MM dd yyyy", "yyyy MM dd", "yyyy dd MM", "dd.MM.yyyy", "MM.dd.yyyy",
                    "yyyy.MM.dd", "yyyy.dd.MM","yyyyMMdd","yyyyddMM","MMddyyyy","ddMMyyyy"
                };//add your own to the array if any

                culture.DateTimeFormat.SetAllDateTimePatterns(dateFormats, 'Y');

                if (DateTime.ParseExact(d, "MM/dd/yyyy", culture, DateTimeStyles.None) != null)
                {
                    DateTime date = DateTime.ParseExact(d, "MM/dd/yyyy", culture, DateTimeStyles.None);
                    if (isEndDate)
                        date = date.AddDays(1);

                    return date;
                }
                return null;
            }
        }
    }
}