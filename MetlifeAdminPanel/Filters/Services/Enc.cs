﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace MetlifeAdminPanel.Services
{
    public class Enc
    {
        private static readonly Encoding encoding = Encoding.UTF8;
        //private static string key = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345678912";
        private static string key2 = "8UHjPgXZzXCGkhxV2QCnooyJexUzvSha";
        private static SHA256 mySHA256 = SHA256Managed.Create();

        // Create secret IV
        private static byte[] iv = new byte[16] { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 };

        public  byte[] Encrypt(string plainText)
        {
            if (plainText == null)
            {
                plainText = "";
            }

            byte[] key;
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                key = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(key2));

            }


            // byte[] key = Encoding.ASCII.GetBytes(key2);
            //byte[] key = mySHA256.ComputeHash(Encoding.ASCII.GetBytes(key2));
            // Instantiate a new Aes object to perform string symmetric encryption
            Aes encryptor = Aes.Create();

            encryptor.Mode = CipherMode.CBC;
            encryptor.KeySize = 256;
            //encryptor.BlockSize = 128;
            //encryptor.Padding = PaddingMode.Zeros;

            // Set key and IV
            encryptor.Key = key;
            encryptor.IV = iv;

            // Instantiate a new MemoryStream object to contain the encrypted bytes
            MemoryStream memoryStream = new MemoryStream();

            // Instantiate a new encryptor from our Aes object
            ICryptoTransform aesEncryptor = encryptor.CreateEncryptor();

            // Instantiate a new CryptoStream object to process the data and write it to the 
            // memory stream
            CryptoStream cryptoStream = new CryptoStream(memoryStream, aesEncryptor, CryptoStreamMode.Write);

            // Convert the plainText string into a byte array
            byte[] plainBytes = Encoding.ASCII.GetBytes(plainText);

            // Encrypt the input plaintext string
            cryptoStream.Write(plainBytes, 0, plainBytes.Length);

            // Complete the encryption process
            cryptoStream.FlushFinalBlock();

            // Convert the encrypted data from a MemoryStream to a byte array
            byte[] cipherBytes = memoryStream.ToArray();

            // Close both the MemoryStream and the CryptoStream
            memoryStream.Close();
            cryptoStream.Close();

            return cipherBytes;

            // Convert the encrypted byte array to a base64 encoded string
            /*string cipherText = Convert.ToBase64String(cipherBytes, 0, cipherBytes.Length);

            // Return the encrypted data as a string
            return cipherText;*/
        }

        public  string Decrypt(byte[] cipherText)
        {
            byte[] key;
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                key = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(key2));

            }
            //byte[] key = Encoding.ASCII.GetBytes(key2);
            //byte[] key = mySHA256.ComputeHash(Encoding.ASCII.GetBytes(key2));
            // Instantiate a new Aes object to perform string symmetric encryption
            Aes encryptor = Aes.Create();

            encryptor.Mode = CipherMode.CBC;
            encryptor.KeySize = 256;
            //encryptor.BlockSize = 128;
            //encryptor.Padding = PaddingMode.Zeros;

            // Set key and IV
            encryptor.Key = key;
            encryptor.IV = iv;

            // Instantiate a new MemoryStream object to contain the encrypted bytes
            MemoryStream memoryStream = new MemoryStream();

            // Instantiate a new encryptor from our Aes object
            ICryptoTransform aesDecryptor = encryptor.CreateDecryptor();

            // Instantiate a new CryptoStream object to process the data and write it to the 
            // memory stream
            CryptoStream cryptoStream = new CryptoStream(memoryStream, aesDecryptor, CryptoStreamMode.Write);

            // Will contain decrypted plaintext
            string plainText = String.Empty;

            try
            {
                // Convert the ciphertext string into a byte array
                //byte[] cipherBytes = Convert.FromBase64String(cipherText);
                byte[] cipherBytes = cipherText;

                // Decrypt the input ciphertext string
                cryptoStream.Write(cipherBytes, 0, cipherBytes.Length);

                // Complete the decryption process
                cryptoStream.FlushFinalBlock();

                // Convert the decrypted data from a MemoryStream to a byte array
                byte[] plainBytes = memoryStream.ToArray();

                // Convert the decrypted byte array to string
                plainText = Encoding.ASCII.GetString(plainBytes, 0, plainBytes.Length);
            }
            finally
            {
                // Close both the MemoryStream and the CryptoStream
                memoryStream.Close();
                cryptoStream.Close();
            }

            // Return the decrypted data as a string
            return plainText;
        }




        //public byte[] Encrypt(string plainText)
        //{
        //    if (plainText == null)
        //    {
        //        plainText = "";
        //    }
        //    byte[] key = mySHA256.ComputeHash(Encoding.ASCII.GetBytes(key2));
        //    // Instantiate a new Aes object to perform string symmetric encryption
        //    Aes encryptor = Aes.Create();

        //    encryptor.Mode = CipherMode.CBC;
        //    //encryptor.KeySize = 256;
        //    //encryptor.BlockSize = 128;
        //    //encryptor.Padding = PaddingMode.Zeros;

        //    // Set key and IV
        //    encryptor.Key = key;
        //    encryptor.IV = iv;

        //    // Instantiate a new MemoryStream object to contain the encrypted bytes
        //    MemoryStream memoryStream = new MemoryStream();

        //    // Instantiate a new encryptor from our Aes object
        //    ICryptoTransform aesEncryptor = encryptor.CreateEncryptor();

        //    // Instantiate a new CryptoStream object to process the data and write it to the 
        //    // memory stream
        //    CryptoStream cryptoStream = new CryptoStream(memoryStream, aesEncryptor, CryptoStreamMode.Write);

        //    // Convert the plainText string into a byte array
        //    byte[] plainBytes = Encoding.ASCII.GetBytes(plainText);

        //    // Encrypt the input plaintext string
        //    cryptoStream.Write(plainBytes, 0, plainBytes.Length);

        //    // Complete the encryption process
        //    cryptoStream.FlushFinalBlock();

        //    // Convert the encrypted data from a MemoryStream to a byte array
        //    byte[] cipherBytes = memoryStream.ToArray();

        //    // Close both the MemoryStream and the CryptoStream
        //    memoryStream.Close();
        //    cryptoStream.Close();

        //    return cipherBytes;

        //    // Convert the encrypted byte array to a base64 encoded string
        //    /*string cipherText = Convert.ToBase64String(cipherBytes, 0, cipherBytes.Length);

        //    // Return the encrypted data as a string
        //    return cipherText;*/
        //}

        //public  string Decrypt(byte[] cipherText)
        //{
        //    byte[] key = mySHA256.ComputeHash(Encoding.ASCII.GetBytes(key2));
        //    // Instantiate a new Aes object to perform string symmetric encryption
        //    Aes encryptor = Aes.Create();

        //    encryptor.Mode = CipherMode.CBC;
        //    //encryptor.KeySize = 256;
        //    //encryptor.BlockSize = 128;
        //    //encryptor.Padding = PaddingMode.Zeros;

        //    // Set key and IV
        //    encryptor.Key = key;
        //    encryptor.IV = iv;

        //    // Instantiate a new MemoryStream object to contain the encrypted bytes
        //    MemoryStream memoryStream = new MemoryStream();

        //    // Instantiate a new encryptor from our Aes object
        //    ICryptoTransform aesDecryptor = encryptor.CreateDecryptor();

        //    // Instantiate a new CryptoStream object to process the data and write it to the 
        //    // memory stream
        //    CryptoStream cryptoStream = new CryptoStream(memoryStream, aesDecryptor, CryptoStreamMode.Write);

        //    // Will contain decrypted plaintext
        //    string plainText = String.Empty;

        //    try
        //    {
        //        // Convert the ciphertext string into a byte array
        //        //byte[] cipherBytes = Convert.FromBase64String(cipherText);
        //        byte[] cipherBytes = cipherText;

        //        // Decrypt the input ciphertext string
        //        cryptoStream.Write(cipherBytes, 0, cipherBytes.Length);

        //        // Complete the decryption process
        //        cryptoStream.FlushFinalBlock();

        //        // Convert the decrypted data from a MemoryStream to a byte array
        //        byte[] plainBytes = memoryStream.ToArray();

        //        // Convert the decrypted byte array to string
        //        plainText = Encoding.ASCII.GetString(plainBytes, 0, plainBytes.Length);
        //    }
        //    finally
        //    {
        //        // Close both the MemoryStream and the CryptoStream
        //        memoryStream.Close();
        //        cryptoStream.Close();
        //    }

        //    // Return the decrypted data as a string
        //    return plainText;
        //}


        //public string key = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012345678912";

        /*
        public byte[] Encrypt(string toEncrypt)
        {
            bool useHashing = true;
            byte[] keyArray;
            var toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);
            if (useHashing)
            {
                var hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
                keyArray = Encoding.UTF8.GetBytes(key);

            var aes = new AesCryptoServiceProvider
            {
                KeySize = 256,
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };
            var cTransform = aes.CreateEncryptor();
            var resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            aes.Clear();
            return resultArray;
            //return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }      

        public string Decrypt(byte[] cipherByte)
        {
            // byte to string 
            //string cipherString = System.Text.Encoding.UTF8.GetString(cipherByte);
            bool useHashing = true;
            byte[] keyArray;
            //var toEncryptArray = Convert.FromBase64String(cipherString);
            var settingsReader = new AppSettingsReader();
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(Encoding.UTF8.GetBytes(key));
                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = Encoding.UTF8.GetBytes(key);
            }

            var tdes = new AesCryptoServiceProvider
            {
                KeySize = 256,
                // IV = keyArray,
                Key = keyArray,
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            };            
            var cTransform = tdes.CreateDecryptor();
            var resultArray = cTransform.TransformFinalBlock(cipherByte, 0, cipherByte.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //return the Clear decrypted TEXT
            return Encoding.UTF8.GetString(resultArray);
        }*/




    }
}