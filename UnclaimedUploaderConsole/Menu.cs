//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnclaimedUploaderConsole
{
    using System;
    using System.Collections.Generic;
    
    public partial class Menu
    {
        public Menu()
        {
            this.Access_Logs = new HashSet<Access_Logs>();
            this.Permissions = new HashSet<Permission>();
        }
    
        public int ID { get; set; }
        public string URL { get; set; }
        public Nullable<int> PARENT_MENU { get; set; }
        public Nullable<int> IS_ACTIVE { get; set; }
    
        public virtual ICollection<Access_Logs> Access_Logs { get; set; }
        public virtual ICollection<Permission> Permissions { get; set; }
    }
}
