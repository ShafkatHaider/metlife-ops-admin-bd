//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UnclaimedUploaderConsole
{
    using System;
    using System.Collections.Generic;
    
    public partial class User_Info
    {
        public User_Info()
        {
            this.Access_Logs = new HashSet<Access_Logs>();
            this.User_Role = new HashSet<User_Role>();
        }
    
        public int ID { get; set; }
        public string USERID { get; set; }
        public string PASSWORD { get; set; }
        public string NAME { get; set; }
        public string EMAIL { get; set; }
        public string MOBILE { get; set; }
        public string TYPE { get; set; }
        public string STATUS { get; set; }
        public string LOGIN_TYPE { get; set; }
        public System.DateTime CREATE_TIME { get; set; }
        public Nullable<System.DateTime> UPDATE_TIME { get; set; }
    
        public virtual ICollection<Access_Logs> Access_Logs { get; set; }
        public virtual ICollection<User_Role> User_Role { get; set; }
    }
}
