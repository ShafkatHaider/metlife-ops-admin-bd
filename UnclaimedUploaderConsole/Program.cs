﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace UnclaimedUploaderConsole
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {

                OLPABDDATAEntities db = new OLPABDDATAEntities();
                Enc enc = new Enc();
                
                double elapsed_time = 0;
                List<UnclaimedMaturityLog> listPendingUnclaimedMaturityLog = GetPendingMaturityFileList();
                Console.WriteLine("Found Files. Total: " + listPendingUnclaimedMaturityLog.Count);
                if (listPendingUnclaimedMaturityLog != null)
                {
                    foreach (UnclaimedMaturityLog unclaimedMaturityLog in listPendingUnclaimedMaturityLog)
                    {
                        string path = ConfigurationManager.AppSettings["FILE_PATH"] + "/UploadedFiles/" + unclaimedMaturityLog.FileName;
                        //string path = Server.MapPath("~/UploadedFiles/") + unclaimedMaturityLog.FileName;
                        Console.WriteLine("File Path: "+ path);

                        OleDbConnection con_excel = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=Excel 8.0");
                        if (con_excel.State == ConnectionState.Closed)
                            con_excel.Open();

                        OleDbCommand cmd = new OleDbCommand("SELECT * FROM [Sheet1$]", con_excel);
                        OleDbDataReader reader = cmd.ExecuteReader();
                        
                        if (reader.HasRows)
                        {
                            OLPABDDATAEntities context = new OLPABDDATAEntities();
                            List<UnclaimedMaturityData> dataList = new List<UnclaimedMaturityData>();

                            context.Configuration.AutoDetectChangesEnabled = false; //new
                            var stopwatch = new Stopwatch();
                            stopwatch.Start();




                            Console.WriteLine("Start Reading from File");
                            int i = 0;
                            while (reader.Read())
                            {

                                string POLICY_NO = reader.GetValue(0).ToString();
                                string POLICY_OWNER_NAME = reader.GetValue(1).ToString();
                                string YEAR_OF_BIRTH = reader.GetValue(2).ToString();
                                string AMOUNT = reader.GetValue(3).ToString();
                                string BENEFIT_TYPE = reader.GetValue(4).ToString();
                                UnclaimedMaturityData unclaimedMaturityData = new UnclaimedMaturityData();
                                unclaimedMaturityData.BatchID = unclaimedMaturityLog.BachId.ToString();
                                unclaimedMaturityData.PolicyNo = enc.Encrypt(POLICY_NO);
                                unclaimedMaturityData.POName = enc.Encrypt(POLICY_OWNER_NAME);
                                unclaimedMaturityData.YearOfBirth = enc.Encrypt(YEAR_OF_BIRTH);
                                unclaimedMaturityData.Amount = Convert.ToDecimal(AMOUNT);
                                unclaimedMaturityData.BenefitType = BENEFIT_TYPE;
                                unclaimedMaturityData.CreationTime = DateTime.Now;
                                dataList.Add(unclaimedMaturityData);
                                //Console.WriteLine("Insert Number "+ i+1 +"Inserting Policy :"+ POLICY_NO);
                                //db.UnclaimedMaturityDatas.Add(unclaimedMaturityData);
                                //db.SaveChanges();
                                i++;
                            }

                            Console.WriteLine("Total: " + dataList.Count);


                            #region Calculation 
                            //Calculation Function
                            int uploadedcount = 0;
                            List<UnclaimedMaturityData> accounts = new List<UnclaimedMaturityData>();
                            try
                            {
                                
                                foreach (var account in dataList)
                                {
                                    accounts.Add(account);
                                    if (accounts.Count % 10000 == 0)
                                    // Play with this number to see what works best
                                    {
                                        Console.WriteLine("Inserting 10000.");
                                        int newuploadcount = accounts.Count;
                                        newuploadcount = newuploadcount + uploadedcount;
                                        uploadedcount = newuploadcount;
                                        context.Configuration.AutoDetectChangesEnabled = false;
                                        context.Configuration.ValidateOnSaveEnabled = false;
                                        context.UnclaimedMaturityDatas.AddRange(accounts);
                                        //context.ChangeTracker.DetectChanges();
                                        context.SaveChanges();
                                        context.Dispose();
                                        accounts = new List<UnclaimedMaturityData>();
                                        context = new OLPABDDATAEntities();
                                       // uploadedcount = uploadedcount + 1;
                                    }
                                }

                                Console.WriteLine("Inserting Rest: Total." + accounts.Count);
                                uploadedcount = uploadedcount + accounts.Count;
                                context.UnclaimedMaturityDatas.AddRange(accounts);
                                context.ChangeTracker.DetectChanges();
                                context.SaveChanges();
                                context.Dispose();
                                //uploadedcount = uploadedcount + 1;
                            }
                            catch (Exception ex)
                            {

                            }

                            #endregion


                            //db.UnclaimedMaturityData.AddRange(dataList);
                            //db.SaveChanges();
                            unclaimedMaturityLog.Status = "SUCCESS";
                            unclaimedMaturityLog.TotalUploaded = uploadedcount;
                            db.Entry(unclaimedMaturityLog).State = EntityState.Modified;
                            db.SaveChanges();
                            reader.Close();
                            //new

                            stopwatch.Stop();
                            elapsed_time = stopwatch.Elapsed.TotalMinutes;
                        }
                        cmd.Dispose();
                        con_excel.Close();
                        con_excel.Dispose();
                    }

                    Console.WriteLine("elapsed time: " + elapsed_time.ToString() + " Minutes");
                    //ViewBag.Message = "SUCCESS" + elapsed_time.ToString();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error :: "+ ex.Message.ToString());
            }

            Console.WriteLine("Done.....");
            Console.ReadLine();
        }
        public static List<Response> GetObjectMessage()
        {
            OLPABDDATAEntities db = new OLPABDDATAEntities();
            List<Response> responses = new List<Response>();

            List<UnclaimedMaturityData> accounts = new List<UnclaimedMaturityData>();
            //foreach (var account in dataList)
            //{
            //    accounts.Add(account);
            //    if (accounts.Count % 10000 == 0)
            //    // Play with this number to see what works best
            //    {
            //        Console.WriteLine("Inserting 10000.");
            //        context.Configuration.AutoDetectChangesEnabled = false;
            //        context.Configuration.ValidateOnSaveEnabled = false;
            //        context.UnclaimedMaturityDatas.AddRange(accounts);
            //        //context.ChangeTracker.DetectChanges();
            //        context.SaveChanges();
            //        context.Dispose();
            //        accounts = new List<UnclaimedMaturityData>();
            //        context = new OLPABDDATAEntities();
            //    }
            //}
            return responses;
        }
        public static List<UnclaimedMaturityLog> GetPendingMaturityFileList()
        {
            OLPABDDATAEntities db = new OLPABDDATAEntities();
            List<UnclaimedMaturityLog> listUnclaimedMaturityLog = db.UnclaimedMaturityLogs.Where(b => b.Status == "processing").ToList();
            return listUnclaimedMaturityLog;
        }
    }
}
